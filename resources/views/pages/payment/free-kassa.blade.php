@extends('layouts.payment')

@section('payment_method', 'GET')
@section('payment_url', 'http://www.free-kassa.ru/merchant/cash.php')

@section('form_body')
    <input type='hidden' name='m'  value="{{ config('payment.free_kassa.merchant_id') }}" />
    <input type='hidden' name='o'  value="{{ $fields['payment_id'] }}" />
    <input type='hidden' name='s'  value="{{ $fields['signature'] }}" />
    <input type="hidden" name="oa" value="{{ $fields['amount'] }}"/>
@endsection