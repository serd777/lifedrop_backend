<!DOCTYPE HTML>

<html>
    <head>
        <meta charset="UTF-8" />
        <title>Перенаправление на сайт платежной системы...</title>

        <style>
            * {
                margin: 0;
                padding: 0;
            }
            html, body {
                background: #1A1A1A;
            }
            body {
                height: 100%;
            }

            .redirect {
                font-size: 30px;
                color: #FFF;
                font-family: Helvetica, Arial, sans-serif;
                line-height: 100%;
                text-align: center;
                position: absolute;
                width: 100%;
                top: calc(50% - 30px);
            }
        </style>
    </head>

    <body>
        <div class="redirect">Перенаправление на сайт платежного оператора...</div>

        <form id="PayForm" method="@yield('payment_method')" action="@yield('payment_url')">
            @yield('form_body')
        </form>

        <script>
            function ready(fn) {
                if (document.readyState != 'loading')
                    fn();
                else
                    document.addEventListener('DOMContentLoaded', fn);
            }
            ready(function(){
                document.getElementById('PayForm').submit();
            });
        </script>
    </body>
</html>