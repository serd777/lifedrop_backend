<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoxesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boxes', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name', 64);
            $table->string('img_path', 256)->nullable();
            $table->unsignedInteger('price')->default(0);

            $table->boolean('is_enabled')->default(true);

            $table->unsignedInteger('section_id');
            $table->foreign('section_id')
                ->references('id')
                ->on('boxes_sections');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boxes');
    }
}
