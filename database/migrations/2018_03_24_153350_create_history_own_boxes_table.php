<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoryOwnBoxesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_own_boxes', function (Blueprint $table) {
            $table->increments('id');

	        $table->unsignedInteger('mentor_id');
	        $table->foreign('mentor_id')
	              ->references('id')
	              ->on('users');

	        $table->unsignedInteger('user_id');
	        $table->foreign('user_id')
	              ->references('id')
	              ->on('users');

	        $table->unsignedInteger('box_id');
	        $table->foreign('box_id')
	              ->references('id')
	              ->on('own_boxes');

	        $table->unsignedInteger('amount');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_own_boxes');
    }
}
