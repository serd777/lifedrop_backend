<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOwnBoxesItemsAvailableTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('own_boxes_items_available', function (Blueprint $table) {
            $table->increments('id');

	        $table->unsignedInteger('item_id');
	        $table->foreign('item_id')
	              ->references('id')
	              ->on('items');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('own_boxes_items_available');
    }
}
