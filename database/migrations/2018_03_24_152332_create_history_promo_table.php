<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoryPromoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_promo', function (Blueprint $table) {
            $table->increments('id');

	        $table->unsignedInteger('user_id');
	        $table->foreign('user_id')
	              ->references('id')
	              ->on('users');

	        $table->unsignedInteger('code_id');
	        $table->foreign('code_id')
	              ->references('id')
	              ->on('promo_codes');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_promo');
    }
}
