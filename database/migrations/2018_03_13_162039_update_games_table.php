<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('games', function (Blueprint $table) {
	        $table->unsignedInteger('box_id')->nullable()->change();

	        $table->unsignedInteger('own_box_id')->nullable();
	        $table->foreign('own_box_id')->references('id')->on('own_boxes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('games', function (Blueprint $table) {
	        $table->unsignedInteger('box_id')->change();

	        $table->dropForeign('own_box_id');
	        $table->dropColumn('own_box_id');
        });
    }
}
