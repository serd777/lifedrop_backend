<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', config('app.frontend_url'));
Route::redirect('/login', config('app.frontend_url'))->name('login');

Route::group(['prefix' => 'auth'], function() {
    //  Auth VK Route
    Route::get('vk', 'Auth\VkAuthController@auth')->middleware('guest');
	//  Auth VK Callback Route
	Route::get('vk/callback', 'Auth\VkAuthController@auth')->middleware('guest');
    //  Auth Logout Route
    Route::get('logout', function() {
        \Auth::logout();
        return redirect(config('app.frontend_url'));
    })->middleware('auth');
});

Route::get('/ref/{mentor_id}', 'AppModules\UserController@referralInvite');
Route::post('/payment/gateway', 'AppModules\PaymentController@gateway');
Route::any('/payment/handler/free-kassa', 'AppModules\PaymentController@callbackForFreeKassa');

Route::get('/user/info', 'AppModules\UserController@getInfo');
Route::get('/user/csrf_token', 'AppModules\UserController@getCsrfToken');
Route::post('/user/activate_promo', 'AppModules\UserController@activatePromo')->middleware('auth');
Route::post('/user/update_address', 'AppModules\UserController@updateAddress')->middleware('auth');
Route::post('/user/profile', 'AppModules\UserController@getUserProfileInfo');

Route::get('/items/list', 'AppModules\OwnBoxesController@getAllItems');
Route::get('/own_boxes/list_img', 'AppModules\OwnBoxesController@getImages');
Route::post('/own_boxes/create', 'AppModules\OwnBoxesController@createBox')->middleware('auth');
Route::get('/own_boxes/list', 'AppModules\OwnBoxesController@getAllOwnBoxes');
Route::post('/own_boxes/list_items', 'AppModules\OwnBoxesController@getBoxItems');
Route::post('/own_boxes/open', 'AppModules\OwnBoxesController@openBox')->middleware('auth');
Route::get('/own_boxes/stat', 'AppModules\OwnBoxesController@getOwnBoxesStat');

Route::get('/boxes/list', 'AppModules\BoxesController@getBoxes');
Route::post('/boxes/items', 'AppModules\BoxesController@getBoxItems');
Route::get('/offers/list', 'AppModules\OffersController@getOffers');
Route::get('/top_users/list', 'AppModules\TopUserController@getUsers');

Route::post('/game/open_box', 'AppModules\GameController@openBox')->middleware('auth');
Route::post('/game/sell_item', 'AppModules\GameController@sellDrop')->middleware('auth');
Route::post('/game/take_item', 'AppModules\GameController@takeDrop')->middleware('auth');

Route::group([
	'prefix' => 'admin',
	'middleware' => ['auth', 'admin']
], function() {
    Route::get('users/list', 'Admin\UsersController@getUsers');
    Route::post('users/user_info', 'Admin\UsersController@getUserInfo');
    Route::post('users/change_balance', 'Admin\UsersController@changeBalance');
    Route::post('users/change_block', 'Admin\UsersController@updateBlockState');
    Route::post('users/change_rights', 'Admin\UsersController@updatePermissions');
    Route::post('users/get_cheats', 'Admin\UsersController@getCheats');
    Route::post('users/add_cheat', 'Admin\UsersController@addCheat');
    Route::post('users/del_cheat', 'Admin\UsersController@deleteCheat');
    Route::post('users/get_address', 'Admin\UsersController@getUserAddress');
    Route::post('users/user_stat', 'Admin\UsersController@getUserStat');

    Route::get('statistic/info', 'Admin\StatisticController@getSiteStat');

    Route::get('items/list', 'Admin\ItemsController@getItems');
    Route::post('items/add', 'Admin\ItemsController@addItem');
    Route::post('items/delete', 'Admin\ItemsController@deleteItem');
    Route::post('items/update', 'Admin\ItemsController@updateItem');
    Route::post('items/update_img', 'Admin\ItemsController@updateItemImg');

    Route::get('boxes/sections/list', 'Admin\BoxesSectionsController@getSections');
    Route::post('boxes/sections/add', 'Admin\BoxesSectionsController@addSection');
    Route::post('boxes/sections/delete', 'Admin\BoxesSectionsController@deleteSection');
    Route::post('boxes/sections/update', 'Admin\BoxesSectionsController@updateSection');

    Route::get('boxes/list', 'Admin\BoxesController@getBoxes');
    Route::post('boxes/add', 'Admin\BoxesController@addBox');
    Route::post('boxes/delete', 'Admin\BoxesController@deleteBox');
    Route::post('boxes/update', 'Admin\BoxesController@updateBox');
    Route::post('boxes/update_img', 'Admin\BoxesController@updateBoxImg');
    Route::post('boxes/items/list', 'Admin\BoxesController@getBoxItems');
    Route::post('boxes/items/add', 'Admin\BoxesController@addItemToBox');
    Route::post('boxes/items/delete', 'Admin\BoxesController@deleteItemFromBox');
    Route::post('boxes/items/update', 'Admin\BoxesController@updateDropPercent');

    Route::get('promo_offers/list', 'Admin\PromoOfferController@getPromoOffers');
    Route::post('promo_offers/add', 'Admin\PromoOfferController@addPromoOffer');
    Route::post('promo_offers/delete', 'Admin\PromoOfferController@deletePromoOffer');
    Route::post('promo_offers/update', 'Admin\PromoOfferController@updatePromoOfferText');
    Route::post('promo_offers/update_img', 'Admin\PromoOfferController@updatePromoOfferImage');

    Route::get('promo_codes/list', 'Admin\PromoCodesController@getPromoCodes');
    Route::post('promo_codes/add','Admin\PromoCodesController@addPromoCode');
    Route::post('promo_codes/delete', 'Admin\PromoCodesController@deletePromoCode');

    Route::get('payouts/list/waiting', 'Admin\PayoutsController@getWaitingDrops');
    Route::get('payouts/list/collecting', 'Admin\PayoutsController@getCollectingDrops');
    Route::get('payouts/list/sent', 'Admin\PayoutsController@getSentDrops');
    Route::post('payouts/mark_as_collecting', 'Admin\PayoutsController@markAsCollecting');
    Route::post('payouts/mark_as_sent', 'Admin\PayoutsController@markAsSent');
    Route::post('payouts/get_comment', 'Admin\PayoutsController@getComment');
    Route::post('payouts/add_comment', 'Admin\PayoutsController@addComment');

    Route::get('own_boxes/list_img', 'Admin\OwnBoxesController@listImages');
    Route::post('own_boxes/add_img', 'Admin\OwnBoxesController@addImage');
    Route::post('own_boxes/delete_img', 'Admin\OwnBoxesController@deleteImage');
    Route::get('own_boxes/list_available_items', 'Admin\OwnBoxesController@listAvailableItems');
    Route::post('own_boxes/del_avail_item', 'Admin\OwnBoxesController@deleteAvailableItem');
    Route::post('own_boxes/add_avail_item', 'Admin\OwnBoxesController@addAvailableItem');
});