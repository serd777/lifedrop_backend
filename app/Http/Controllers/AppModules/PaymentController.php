<?php namespace App\Http\Controllers\AppModules;

use App\Http\Controllers\AppModules\Payment\FreeKassaModule;
use App\Http\Controllers\Controller;
use App\Http\Models\Payment;
use Illuminate\Http\Request;

/**
 * Платежный контроллер
 * @package App\Http\Controllers\Modules
 */
class PaymentController extends Controller
{
    /**
     * Массив платежных модулей
     * @var array
     */
    private $modules;

    /**
     * Конструктор класса
     */
    public function __construct()
    {
        $this->modules = [];

        $this->modules['free-kassa'] = new FreeKassaModule();
    }

    /**
     * Платежный модуль
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function gateway(Request $request)
    {
        $validation = \Validator::make($request->all(), [
            'type' => 'required|in:free-kassa'
        ]);
        if($validation->fails())
            return \Response::make($validation->errors()->first());

        $data = [];
        $type = $request->input('type');

        $data['merchant_id'] = config("payment.$type.merchant_id", '');
        $data['amount']      = $request->input('amount');
        $data['currency']    = config("payment.$type.currency", '');

        $payment = Payment::create([
            'user_id'      => \Auth::user()->id,
            'amount'       => (is_null($data['amount']) ? 0 : $data['amount']),
            'type'         => $type,
        ]);
        $data['payment_id'] = $payment->id;

        return $this->modules[$type]->generateGateway($data);
    }

    /**
     * Callback для Free Kassa
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function callbackForFreeKassa(Request $request)
    {
        $validation = \Validator::make($request->all(), [
            'AMOUNT'            => 'required',
            'MERCHANT_ORDER_ID' => 'required',
            'SIGN'              => 'required',
            'intid'             => 'required',
        ]);
        if($validation->fails())
            return \Response::make($validation->errors()->first());

        $fields = $request->all();
        $signature = $request->input('SIGN');

        return $this->modules['free-kassa']->callbackAnswer($fields, $signature);
    }
}