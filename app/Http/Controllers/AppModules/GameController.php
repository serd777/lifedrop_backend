<?php namespace App\Http\Controllers\AppModules;

use App\Http\Controllers\Controller;
use App\Http\Models\Box;
use App\Http\Models\Cheat;
use App\Http\Models\Drop;
use App\Http\Models\Game;
use App\Http\Models\Item;
use App\Http\Models\Profit;
use App\Http\RequestsAnswer;
use App\Http\Services\BoxesService;
use App\Http\Services\DropsService;
use App\Http\Services\PayoutsService;
use App\Http\Services\UsersService;
use Illuminate\Http\Request;

class GameController extends Controller
{
    public function openBox(Request $request) {
        $validation = \Validator::make($request->all(), [
            'id' => 'required|numeric'
        ]);
        if($validation->fails())
            return RequestsAnswer::failed($validation->errors()->first());

        $box = Box::find($request->input('id'));
        if(!$box) return RequestsAnswer::failed('Not Found');

        $user = \Auth::user();
        if($user->balance < $box->price)
            return RequestsAnswer::failed('Not Enough Money');

        $user->balance -= $box->price;
        $user->save();

        $cheats = $this->getCheats($user, $box);
        if($cheats->count())
            return RequestsAnswer::success(
                $this->returnCheatItem($user, $box, $cheats)
            );

        return RequestsAnswer::success(
            $this->returnRandomItem($user, $box)
        );
    }

    public function takeDrop(Request $request) {
        $validation = \Validator::make($request->all(), [
            'id' => 'required|numeric',
        ]);
        if($validation->fails())
            return RequestsAnswer::failed($validation->errors()->first());

        $drop = Drop::find($request->input('id'));
        if(!$drop) return RequestsAnswer::failed('NOT FOUND');

        if($drop->user_id != \Auth::user()->id)
        	return RequestsAnswer::failed();

        if($drop->status != Drop::STATUS_UNCHECKED)
            return RequestsAnswer::failed('Already checked');

        PayoutsService::changeStatus($drop, Drop::STATUS_WAITING);
        return RequestsAnswer::success();
    }

    public function sellDrop(Request $request) {
        $validation = \Validator::make($request->all(), [
            'id' => 'required|numeric',
        ]);
        if($validation->fails())
            return RequestsAnswer::failed($validation->errors()->first());

        $drop = Drop::find($request->input('id'));
        if(!$drop) return RequestsAnswer::failed('NOT FOUND');

	    if($drop->user_id != \Auth::user()->id)
		    return RequestsAnswer::failed();

        if($drop->status != Drop::STATUS_UNCHECKED)
            return RequestsAnswer::failed('Already checked');

        PayoutsService::changeStatus($drop, Drop::STATUS_SOLD);

        $user = \Auth::user();
        $user->balance += Item::find($drop->item_id)->price;
        $user->save();

        Profit::create([
            'user_id' => $user->id,
            'item_id' => $drop->item_id,
            'profit'  => Item::find($drop->item_id)->price,
        ]);

        return RequestsAnswer::success();
    }

    private function getCheats($user, $box) {
        $box_items = BoxesService::getBoxItems($box->id);
        return Cheat::ofUser($user->id)
            ->whereIn('item_id', $box_items->pluck('item_id')->toArray())
            ->get();
    }

    private function generateDrop($user, $box, $item) {
        $drop = Drop::create([
            'item_id'       => $item->id,
            'user_id'       => $user->id,
	        'border_color'  => $box->border_color
        ]);
        Game::create([
            'item_id' => $item->id,
            'box_id'  => $box->id,
            'user_id' => $user->id,
        ]);

        DropsService::updateDropsCache($user->id);
        UsersService::updateUserGamesAmount($user->id);

        $drop->item = $item;
        return $drop;
    }

    private function returnCheatItem($user, $box, $cheats) {
        $cheat = $cheats->first();

        $cheat->amount--;
        if($cheat->amount) $cheat->save();
        else $cheat->delete();

        return $this->generateDrop($user, $box, Item::find($cheat->item_id));
    }

    private function returnRandomItem($user, $box) {
        $box_items = BoxesService::getBoxItems($box->id);

        $decimals_max = 0;
        foreach($box_items as $box_item) {
            $nums = $this->numberOfDecimals($box_item->percent);
            if($nums > $decimals_max) $decimals_max = $nums;
        }

        $rand = $this->mt_rand_float(0, 1, $decimals_max);
        $cur_p = 0;

        foreach($box_items as $box_item) {
            if($rand < $cur_p + $box_item->percent)
                return $this->generateDrop($user, $box, Item::find($box_item->item_id));
            $cur_p += $box_item->percent;
        }

        return $this->generateDrop($user, $box, Item::find($box_items->orderBy('percent', 'desc')->first()->item_id));
    }

	private function mt_rand_float($min, $max, $amountZero = 0) {
    	$countZero = '0';
    	for($i = 0 ; $i < $amountZero; $i++)
    		$countZero .= '0';

		$countZero = +('1'.$countZero);
		$min = floor($min*$countZero);
		$max = floor($max*$countZero);
		$rand = mt_rand($min, $max) / $countZero;
		return $rand;
	}

	private function numberOfDecimals($value) {
		if(strpos($value, '.') !== FALSE)
			return strlen($value) - strrpos($value, '.') - 1;
		if(strpos($value, ',') !== FALSE)
			return strlen($value) - strrpos($value, ',') - 1;
		return 0;
	}
}