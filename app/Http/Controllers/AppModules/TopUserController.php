<?php namespace App\Http\Controllers\AppModules;

use App\Http\CacheLabels;
use App\Http\Controllers\Controller;
use App\Http\Models\TopUser;
use App\Http\RequestsAnswer;

class TopUserController extends Controller
{
	public function getUsers()
	{
		if(\Cache::has(CacheLabels::TopUsersList))
			$users = \Cache::get(CacheLabels::TopUsersList);
		else $users = $this->updateCache();

		return RequestsAnswer::success($users);
	}

	private function updateCache()
	{
		$users = TopUser::orderBy('profit', 'desc')->with('user')->take(20)->get();
		\Cache::put(CacheLabels::TopUsersList, $users, 120);
		return $users;
	}
}