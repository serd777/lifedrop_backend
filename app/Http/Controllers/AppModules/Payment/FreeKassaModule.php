<?php namespace App\Http\Controllers\AppModules\Payment;

use App\Http\Controllers\AppModules\UserController;
use App\Http\Models\History\HistoryPayment;
use App\Http\Models\Payment;
use App\Http\Models\User;

class FreeKassaModule implements PaymentInterface
{
	public function generateGateway($data)
	{
		$data['signature'] = $this->getPublicSignature($data['amount'], $data['payment_id']);
		return view('pages.payment.free-kassa', [
			'fields' => $data,
		]);
	}

	public function callbackAnswer($data, $signature)
	{
		$p_sign = $this->getPrivateSignature($data['AMOUNT'], $data['MERCHANT_ORDER_ID']);

		if($p_sign == $data['SIGN']) {
			$payment = Payment::find($data['MERCHANT_ORDER_ID']);

			if(!$payment)
				return \Response::make('NOT FOUND');
			if($payment->payed)
				return \Response::make('ALREADY PAYED');

			if($payment->amount >= 500)
				if( !Payment::ofUser($payment->user_id)->ofPayed()->where('amount', '>=', 500)->count() )
					$payment->amount += round($payment->amount * 0.15);

			$user = User::find($payment->user_id);
			$user->balance += $payment->amount;
			$user->save();

			$payment->remote_id = $data['intid'];
			$payment->payed = true;
			$payment->save();

			HistoryPayment::create([
				'user_id' => $user->id,
				'amount'  => $payment->amount
			]);

			UserController::processReferral($user, $payment->amount);
			return \Response::make('OK');
		}

		return \Response::make('SIGNATURE FAILED');
	}

	/**
	 * Генератор подписи, переданных параметров
	 *
	 * @param $amount
	 * @param $payment_id
	 * @return string
	 */
	private function getPrivateSignature($amount, $payment_id)
	{
		//Получаем входные данные
		$merchant_id  = config('payment.free_kassa.merchant_id');
		$secret_word  = config('payment.free_kassa.secret_2');

		//Генерируем подпись
		$hash = md5($merchant_id . ':' . $amount . ':' . $secret_word . ':' . $payment_id);

		//Возвращаем её
		return $hash;
	}

	/**
	 * Генератор публичной подписи
	 *
	 * @param $amount
	 * @param $payment_id
	 *
	 * @return mixed
	 */
	private function getPublicSignature($amount, $payment_id)
	{
		//Получаем входные данные
		$merchant_id  = config('payment.free_kassa.merchant_id');
		$secret_word  = config('payment.free_kassa.secret_1');

		//Генерируем подпись
		$hash = md5($merchant_id . ':' . $amount . ':' . $secret_word . ':' . $payment_id);

		//Возвращаем подпись
		return $hash;
	}
}