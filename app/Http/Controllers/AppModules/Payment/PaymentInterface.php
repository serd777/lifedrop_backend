<?php namespace App\Http\Controllers\AppModules\Payment;

/**
 * Интерфейс платежного модуля
 * @package App\Http\Controllers\Modules\Payment
 */
interface PaymentInterface
{
    /**
     * Генерация платежного редиректа
     * @param $data - данные для модуля
     * @return mixed
     */
    public function generateGateway($data);

    /**
     * Генерация ответа для callback
     * @param $data - данные для генерации
     * @param $signature - ЭЦП от платежки
     * @return mixed
     */
    public function callbackAnswer($data, $signature);
}