<?php namespace App\Http\Controllers\AppModules;

use App\Http\Controllers\Controller;
use App\Http\Models\History\HistoryReferrals;
use App\Http\Models\PromoCode;
use App\Http\Models\Referral;
use App\Http\Models\User;
use App\Http\Models\UserAddress;
use App\Http\RequestsAnswer;
use App\Http\Services\DropsService;
use App\Http\Services\PromoCodesService;
use App\Http\Services\UsersService;
use App\Http\Services\Prototypes\NewAddress;
use Illuminate\Http\Request;

/**
 * Controller, which process user interactions
 * @package App\Http\Controllers\AppModules
 */
class UserController extends Controller
{
    /**
     * Method, which returns user info
     * @return \Illuminate\Http\JsonResponse
     */
    public function getInfo()
    {
        if(\Auth::guest()) return RequestsAnswer::failed();

        $user = \Auth::user();

        $address = UserAddress::where('user_id', $user->id)->orderBy('created_at', 'desc')->first();
        if(!$address) $address = new UserAddress(['name' => '']);

        return RequestsAnswer::success([
            'user'         => $user,
            'address'      => $address,
            'drops'        => DropsService::getDrops($user->id),
            'games_played' => (int) UsersService::getUserGamesAmount($user->id),
            'profit'       => (int) UsersService::getUserProfit($user->id),
            'referrals'    => (int) UsersService::getUserReferralAmount($user->id)
        ]);
    }

	/**
	 * Method, which returns user's info for profile
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
    public function getUserProfileInfo(Request $request)
    {
    	$validation = \Validator::make($request->all(), [
    		'id' => 'required|numeric'
	    ]);
    	if($validation->fails())
    		return RequestsAnswer::failed($validation->errors()->first());

    	$user = User::find($request->input('id'));
    	if(!$user) return RequestsAnswer::failed();

    	return RequestsAnswer::success([
    		'avatar'       => $user->avatar,
			'name'         => $user->name,
		    'vk_id'        => $user->vk_id,
		    'drops'        => DropsService::getDrops($user->id),
			'games_played' => (int) UsersService::getUserGamesAmount($user->id),
			'profit'       => (int) UsersService::getUserProfit($user->id),
		    'referrals'    => (int) UsersService::getUserReferralAmount($user->id)
	    ]);
    }

    /**
     * Method, which returns Csrf token
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCsrfToken()
    {
        return RequestsAnswer::success(csrf_token());
    }

    /**
     * Method, which activates promo code for user
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function activatePromo(Request $request)
    {
        $validation = \Validator::make($request->all(), [
            'code' => 'required'
        ]);
        if($validation->fails())
            return RequestsAnswer::failed();

        $promo = PromoCode::where('code', $request->input('code'))->first();
        if(!$promo) return RequestsAnswer::failed();

        $user = \Auth::user();
        if(PromoCodesService::isAlreadyActivated($user, $promo))
            return RequestsAnswer::failed();

        $result = 0;
        try {
            $result = PromoCodesService::activatePromo($user, $promo);
        } catch (\Exception $e) {}

        return RequestsAnswer::success($result);
    }

    /**
     * Method, which updates user's address
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateAddress(Request $request)
    {
        $validation = \Validator::make($request->all(), [
            'name'       => 'required',
            'country'    => 'required',
            'region'     => 'required',
            'city'       => 'required',
            'address'    => 'required',
            'phone'      => 'required',
	        'post_index' => 'required',
        ]);
        if($validation->fails())
            return RequestsAnswer::failed($validation->errors()->first());

        ($request->has('additional_info')) ?
            $info = $request->input('additional_info') :
            $info = '';

        $fields = new NewAddress();
        $fields->name    = $request->input('name');
        $fields->country = $request->input('country');
        $fields->region  = $request->input('region');
        $fields->city    = $request->input('city');
        $fields->address = $request->input('address');
        $fields->phone   = $request->input('phone');
        $fields->has_post_index  = true;
        $fields->post_index      = $request->input('post_index');
        $fields->additional_info = $info;

        return RequestsAnswer::success(
            UsersService::updateUserAddress(
                \Auth::user()->id,
                $fields
            )
        );
    }

    public function referralInvite($mentor_id)
    {
    	if(!\Auth::guest())
		    return redirect(env('APP_URL_FRONTEND'));
    	if(!is_numeric($mentor_id))
    		return redirect(env('APP_URL_FRONTEND'));

    	$user = User::find($mentor_id);
    	if(!$user)
		    return redirect(env('APP_URL_FRONTEND'));

    	\Session::put('mentor_invite', $mentor_id);
	    return redirect(env('APP_URL_FRONTEND'));
    }

    public static function processReferral($user, $amount)
    {
	    $referral = Referral::where('user_id', $user->id)->with('mentor')->first();
	    if($referral) {
	    	$bonus = round(intval($amount) * 0.05);

		    $referral->mentor->balance += $bonus;
		    $referral->mentor->save();

		    HistoryReferrals::create([
		    	'mentor_id' => $referral->mentor->id,
			    'user_id'   => $user->id,
			    'amount'    => $bonus
		    ]);
	    }
    }
}