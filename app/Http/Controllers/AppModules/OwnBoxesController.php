<?php namespace App\Http\Controllers\AppModules;

use App\Http\Controllers\Controller;
use App\Http\Models\Drop;
use App\Http\Models\Game;
use App\Http\Models\History\HistoryOwnBox;
use App\Http\Models\Item;
use App\Http\Models\OwnBox;
use App\Http\Models\User;
use App\Http\RequestsAnswer;
use App\Http\Services\DropsService;
use App\Http\Services\OwnBoxesService;
use App\Http\Services\UsersService;
use Illuminate\Http\Request;

class OwnBoxesController extends Controller
{
	public function getImages() {
		return RequestsAnswer::success(OwnBoxesService::getBoxesImages());
	}

	public function getAllItems() {
		return RequestsAnswer::success(OwnBoxesService::getAvailableItems());
	}

	public function getAllOwnBoxes() {
		return RequestsAnswer::success([]);
		//return RequestsAnswer::success(OwnBoxesService::getOwnBoxes());
	}

	public function getOwnBoxesStat() {
		return RequestsAnswer::success([]);
		//return RequestsAnswer::success(OwnBoxesService::getOwnBoxesStat());
	}

	public function getBoxItems(Request $request) {
		$validation = \Validator::make($request->all(), [
			'id' => 'required|numeric',
		]);
		if($validation->fails())
			return RequestsAnswer::failed($validation->errors()->first());

		return RequestsAnswer::success(
			OwnBoxesService::getOwnBoxesItems($request->input('id'))
		);
	}

	public function createBox(Request $request) {
		return RequestsAnswer::failed('Временно не работает!');

		$validation = \Validator::make($request->all(), [
			'name'   => 'required',
			'items'  => 'required|array',
			'img_id' => 'required|numeric',
		]);
		if($validation->fails())
			return RequestsAnswer::failed($validation->errors()->first());

		$name    = $request->input('name');
		$items_a = $request->input('items'); //Массив предметов, переданный от клиента
		$img_id  = $request->input('img_id');

		if(OwnBox::where('name', $name)->first())
			return RequestsAnswer::failed('Кейс с данным именем уже существует!');

		$user = \Auth::user();
		if(UsersService::getUserGamesAmount($user->id) < 5)
			return RequestsAnswer::failed('Вы должны открыть 5 кейсов!');
		if(OwnBoxesService::getUserBoxesTodayAmount($user->id) > 5)
			return RequestsAnswer::failed('Можно создавать только 5 кейсов в день!');

		$items_ids = [];
		foreach($items_a as $elem)
			$items_ids[] = $elem['item']['id'];

		$items = Item::whereIn('id', $items_ids)->get();
		if($items->count() < 5)
			return RequestsAnswer::failed('Необходимо 5 предметов для создания');

		return RequestsAnswer::success(
			OwnBoxesService::createOwnBox($user->id, $name, $items, $img_id)
		);
	}

	public function openBox(Request $request) {
		return RequestsAnswer::failed('Not Enough Money');

		$validation = \Validator::make($request->all(), [
			'id' => 'required|numeric',
		]);
		if($validation->fails())
			return RequestsAnswer::failed($validation->errors()->first());

		$box = OwnBox::find($request->input('id'));
		if(!$box) return RequestsAnswer::failed('Not Found');

		$user = \Auth::user();
		if($user->balance < $box->price)
			return RequestsAnswer::failed('Not Enough Money');

		$user->balance -= $box->price;
		$user->save();

		$this->addToCreator($user, $box);

		return RequestsAnswer::success($this->generateRandomDrop($user, $box));
	}

	private function addToCreator($user, $box) {
		$creator = User::find($box->user_id);

		$amount = round($box->price * 0.05);
		$creator->balance += $amount;
		$creator->save();

		HistoryOwnBox::create([
			'mentor_id' => $creator->id,
			'user_id'   => $user->id,
			'box_id'    => $box->id,
			'amount'    => $amount
		]);
	}

	private function generateRandomDrop($user, $box) {
		$box_items = OwnBoxesService::getOwnBoxesItems($box->id);
		$max_item = $this->findMaxItem($box_items);
		$min_item = $this->findMinItem($box_items);
		$sum = 0;

		//  Rand val = (max_price + 0.1 * max_price) - price_current;
		foreach ($box_items as $box_item) {
			if($box_item->item->price > ($min_item->item->price + $min_item->item->price * 0.5))
				$value = 0;
			else
				$value = $max_item->item->price - $box_item->item->price;
			$sum += $value;

			$box_item->rand_val = $value;
		}

		$rand_v = mt_rand(1, $sum);
		$rangeStart = 1;

		foreach ($box_items as $box_item) {
			$rangeFinish = $rangeStart + $box_item->rand_val;
			if($rand_v >= $rangeStart && $rand_v <= $rangeFinish)
				return $this->generateDrop($user, $box, Item::find($box_item->item_id));
			$rangeStart = $rangeFinish + 1;
		}

		//Если не попали, то вернем самый дешевый
		return $this->generateDrop($user, $box, Item::find($min_item->item_id));
	}

	private function findMaxItem($box_items) {
		$max = $box_items->first();

		foreach($box_items as $item)
			if($max->item->price < $item->item->price)
				$max = $item;

		return $max;
	}

	private function findMinItem($box_items) {
		$min = $box_items->first();

		foreach($box_items as $item)
			if( $item->item->price < $min->item->price )
				$min = $item;

		return $min;
	}

	private function generateDrop($user, $box, $item) {
		$drop = Drop::create([
			'item_id' => $item->id,
			'user_id' => $user->id
		]);
		Game::create([
			'user_id'    => $user->id,
			'item_id'    => $item->id,
			'own_box_id' => $box->id
		]);

		DropsService::updateDropsCache($user->id);
		UsersService::updateUserGamesAmount($user->id);

		$drop->item = $item;
		return $drop;
	}
}