<?php namespace App\Http\Controllers\AppModules;

use App\Http\Controllers\Controller;
use App\Http\Models\Box;
use App\Http\RequestsAnswer;
use App\Http\Services\BoxesService;
use Illuminate\Http\Request;

class BoxesController extends Controller
{
	public function getBoxes() {
		return RequestsAnswer::success(BoxesService::getActiveBoxes());
	}

	public function getBoxItems(Request $request) {
	    $validation = \Validator::make($request->all(), [
	       'id' => 'required|numeric',
        ]);
	    if($validation->fails())
	        return RequestsAnswer::failed($validation->errors()->first());

	    $box = Box::find($request->input('id'));
	    if(!$box) return RequestsAnswer::failed('NOT FOUND');

	    return RequestsAnswer::success(BoxesService::getBoxItems($request->input('id')));
    }
}