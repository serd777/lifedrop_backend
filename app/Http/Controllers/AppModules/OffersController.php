<?php namespace App\Http\Controllers\AppModules;

use App\Http\Controllers\Controller;
use App\Http\RequestsAnswer;
use App\Http\Services\PromoOfferService;

class OffersController extends Controller
{
	public function getOffers()
	{
		return RequestsAnswer::success(PromoOfferService::getPromoOffers());
	}
}