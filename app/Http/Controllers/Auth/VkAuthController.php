<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Models\Referral;
use App\Http\Models\User;
use App\Http\Services\UsersService;

/**
 * Vk Auth Controller
 * @package App\Http\Controllers\Auth
 */
class VkAuthController extends Controller
{
    /**
     * OAuth Service
     *
     * @var \OAuth
     */
    private $service;

    /**
     * VkAuthController constructor.
     */
    public function __construct()
    {
        $this->service = \OAuth::consumer('Vkontakte');
    }

    /**
     * Auth method
     *
     * @return mixed
     */
    public function auth()
    {
        $code = \Request::get('code');

        if(!$code)
            return $this->externalRedirect();

        return $this->authUser($code);
    }

    /**
     * Redirect to external provider
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    private function externalRedirect()
    {
        return redirect((string)$this->service->getAuthorizationUri());
    }

    /**
     * Auth internal method
     *
     * @param $code
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    private function authUser($code)
    {
        //Getting access token from service
        $token = $this->service->requestAccessToken($code);
        //Getting extra params
        $params = $token->getExtraParams();

        //Sending request to VK provider
        $request = $this->service->request($this->getRequestUrl($params));
        //Decoding answer
        $answer = json_decode($request, true);

        //Getting user info
        $user_info = $answer['response'][0];
        //Trying to find in DB
        $user = User::ofVk($user_info['id'])->first();

        //If this is first time login
        //Then we'll create in DB
        //Otherwise will update info
        if(!$user)
            $user = $this->registerUser($user_info);
        else if($this->needUpdateInfo($user, $user_info))
            $user = $this->updateInfo($user, $user_info);

        //Authenticating user on site
        \Auth::login($user);

        //Redirecting user to main page
        return redirect(config('app.frontend_url'));
    }

    /**
     * Method, which returns request url
     *
     * @param $params
     * @return string
     */
    private function getRequestUrl($params)
    {
        return '/users.get?user_id=' . $params['user_id'] . '&fields=photo_max,photo_max_orig&v=5.73';
    }

    /**
     * Method, which creates user in DB
     *
     * @param $data
     * @return User
     */
    private function registerUser($data)
    {
        $user = User::create([
            'vk_id'        => $data['id'],
            'name'         => $data['first_name'] . ' ' . $data['last_name'],
            'avatar'       => $data['photo_max_orig']
        ]);
        UsersService::updateUsersCache();

        if(\Session::has('mentor_invite'))
            $this->processMentorInvite($user);

        return $user;
    }

    /**
     * Method, which processes mentor invite
     *
     * @param $user - User
     */
    private function processMentorInvite($user)
    {
        $mentor_id = \Session::get('mentor_invite');
        if(!is_numeric($mentor_id)) return;

        $mentor = User::find($mentor_id);
        if(!$mentor) return;

        Referral::create([
            'user_id'   => $user->id,
            'mentor_id' => $mentor_id
        ]);
        UsersService::updateUserReferralsCount($mentor_id);
        \Session::forget('mentor_invite');
    }

    /**
     * Method, which checks user info on change
     *
     * @param $user
     * @param $data
     * @return bool
     */
    private function needUpdateInfo($user, $data)
    {
        //Checking user name
        if($user->name != ($data['first_name'] . ' ' . $data['last_name']))
            return true;
        //Checking avatar
        if($user->avatar != $data['photo_max_orig'])
            return true;
        return false;
    }

    /**
     * Method, which updates user info
     *
     * @param $user
     * @param $data
     * @return User
     */
    private function updateInfo($user, $data)
    {
        //Updating info in DB
        $user->name   = $data['first_name'] . ' ' . $data['last_name'];
        $user->avatar = $data['photo_max_orig'];
        $user->save();

        return $user;
    }
}