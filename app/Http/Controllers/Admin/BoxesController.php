<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Models\Box;
use App\Http\Models\BoxItem;
use App\Http\Models\BoxSection;
use App\Http\Models\Item;
use App\Http\RequestsAnswer;
use App\Http\Services\BoxesService;
use App\Http\Services\NewBox;
use Illuminate\Http\Request;

/**
 * Controller, which controls boxes
 * @package App\Http\Controllers\Admin
 */
class BoxesController extends Controller
{
    /**
     * Method, which returns boxes list
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBoxes()
    {
        return RequestsAnswer::success(BoxesService::getBoxes());
    }

    /**
     * Method, which returns box'es items
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBoxItems(Request $request)
    {
        $validation = \Validator::make($request->all(), [
            'id' => 'required|numeric'
        ]);
        if($validation->fails())
            return RequestsAnswer::failed($validation->errors()->first());

        return RequestsAnswer::success(BoxesService::getBoxItems($request->input('id')));
    }

    /**
     * Method, which adds new box
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addBox(Request $request)
    {
        $validation = \Validator::make($request->all(), [
            'name'          => 'required',
            'img'           => 'required',
            'price'         => 'required|numeric',
            'color_num'     => 'required|numeric',
            'section_id'    => 'required|numeric',
            'pos'           => 'required|numeric',
	        'border_color'  => 'required',
        ]);
        if($validation->fails())
            return RequestsAnswer::failed($validation->errors()->first());

        $section = BoxSection::find($request->input('section_id'));
        if(!$section) return RequestsAnswer::failed();

        $new_box = new NewBox();
        $new_box->name = $request->input('name');
        $new_box->price = $request->input('price');
        $new_box->img = $request->input('img');
        $new_box->color_num = $request->input('color_num');
        $new_box->pos = $request->input('pos');
        $new_box->section = $section;
        $new_box->border_color = $request->input('border_color');
        $new_box->is_priority = ($request->has('is_priority') ? $request->input('is_priority') : false);

        return RequestsAnswer::success(BoxesService::addBox($new_box));
    }

    /**
     * Method, which deletes box from DB
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteBox(Request $request)
    {
        $validation = \Validator::make($request->all(), [
            'id' => 'required|numeric'
        ]);
        if($validation->fails())
            return RequestsAnswer::failed($validation->errors()->first());

        BoxesService::deleteBox($request->input('id'));
        return RequestsAnswer::success();
    }

    /**
     * Method, which updates box in DB
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateBox(Request $request)
    {
        $validation = \Validator::make($request->all(), [
            'id'            => 'required|numeric',
            'name'          => 'required',
            'price'         => 'required|numeric',
            'is_enabled'    => 'required|boolean',
            'color_num'     => 'required|numeric',
            'pos'           => 'required|numeric',
            'section_id'    => 'required|numeric',
	        'border_color'  => 'required',
	        'is_priority'   => 'required|boolean',
        ]);
        if($validation->fails())
            return RequestsAnswer::failed($validation->errors()->first());

        $section = BoxSection::find($request->input('section_id'));
        if(!$section) RequestsAnswer::failed();

        $box = Box::find($request->input('id'));
        if(!$box) RequestsAnswer::failed();

        return RequestsAnswer::success(
            BoxesService::updateBox(
                $box,
                $request->input('name'),
                $request->input('price'),
                $request->input('is_enabled'),
                $request->input('color_num'),
                $request->input('pos'),
                $section,
                $request->input('border_color'),
                $request->input('is_priority')
            )
        );
    }

    public function updateBoxImg(Request $request)
    {
	    $validation = \Validator::make($request->all(), [
		    'id'  => 'required|numeric',
		    'img' => 'required',
	    ]);
	    if($validation->fails())
		    return RequestsAnswer::failed($validation->errors()->first());

	    $box = Box::find($request->input('id'));
	    if(!$box) return RequestsAnswer::failed();

	    return RequestsAnswer::success(
	    	BoxesService::updateBoxImg($box, $request->input('img'))
	    );
    }

    /**
     * Method, which adds item to box
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addItemToBox(Request $request)
    {
        $validation = \Validator::make($request->all(), [
            'box_id'  => 'required|numeric',
            'item_id' => 'required|numeric',
            'percent' => 'required|numeric|min:0|max:100'
        ]);
        if($validation->fails())
            return RequestsAnswer::failed($validation->errors()->first());

        $box = Box::find($request->input('box_id'));
        if(!$box) RequestsAnswer::failed();

        $item = Item::find($request->input('item_id'));
        if(!$item) RequestsAnswer::failed();

        return RequestsAnswer::success(
            BoxesService::addItemToBox(
                $box,
                $item,
                $request->input('percent')
            )
        );
    }

    /**
     * Method, which deletes item from box
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteItemFromBox(Request $request)
    {
        $validation = \Validator::make($request->all(), [
            'id' => 'required|numeric',
        ]);
        if($validation->fails())
            return RequestsAnswer::failed($validation->errors()->first());

        $box_item = BoxItem::find($request->input('id'));
        if(!$box_item) RequestsAnswer::failed();

        try { BoxesService::deleteItemFromBox($box_item); }
        catch (\Exception $e) {}

        return RequestsAnswer::success();
    }

    /**
     * Method, which updates drop percent
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateDropPercent(Request $request)
    {
        $validation = \Validator::make($request->all(), [
            'id'      => 'required|numeric',
            'percent' => 'required|numeric',
        ]);
        if($validation->fails())
            return RequestsAnswer::failed($validation->errors()->first());

        $box_item = BoxItem::find($request->input('id'));
        if(!$box_item) RequestsAnswer::failed();

        return RequestsAnswer::success(
            BoxesService::updateItemPercent(
                $box_item,
                $request->input('percent')
            )
        );
    }
}