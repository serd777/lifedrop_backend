<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Models\Item;
use App\Http\RequestsAnswer;
use App\Http\Services\ItemsService;
use Illuminate\Http\Request;

/**
 * Controller which interacts with items
 * @package App\Http\Controllers\Admin
 */
class ItemsController extends Controller
{
    /**
     * Method, which returns items list
     * @return \Illuminate\Http\JsonResponse
     */
    public function getItems()
    {
        return RequestsAnswer::success(ItemsService::getItems());
    }

    /**
     * Method, which adds new item
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addItem(Request $request)
    {
        $validation = \Validator::make($request->all(), [
            'name'  => 'required',
            'price' => 'required|numeric',
            'img'   => 'required',
        ]);
        if($validation->fails())
            return RequestsAnswer::failed($validation->errors()->first());

        return RequestsAnswer::success(
            ItemsService::addItem(
                $request->input('name'),
	            $request->input('img'),
	            $request->input('price')
            )
        );
    }

    /**
     * Method, which deletes item from DB
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteItem(Request $request)
    {
        $validation = \Validator::make($request->all(), [
            'id' => 'required|numeric',
        ]);
        if($validation->fails())
            return RequestsAnswer::failed($validation->errors()->first());

        ItemsService::deleteItem($request->input('id'));
        return RequestsAnswer::success();
    }

    /**
     * Method, which updates item in DB
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateItem(Request $request)
    {
        $validation = \Validator::make($request->all(), [
            'id'    => 'required|numeric',
            'name'  => 'required',
            'price' => 'required|numeric',
        ]);
        if($validation->fails())
            return RequestsAnswer::failed($validation->errors()->first());

        $item = Item::find($request->input('id'));
        if(!$item) return RequestsAnswer::failed();

        return RequestsAnswer::success(
            ItemsService::updateItem(
                $item,
                $request->input('name'),
                $request->input('price')
            )
        );
    }

    public function updateItemImg(Request $request)
    {
    	$validation = \Validator::make($request->all(), [
    		'id'  => 'required|numeric',
		    'img' => 'required',
	    ]);
    	if($validation->fails())
    		return RequestsAnswer::failed($validation->errors()->first());

		$item = Item::find($request->input('id'));
		if(!$item) return RequestsAnswer::failed();

		return RequestsAnswer::success(
			ItemsService::updateItemImg($item, $request->input('img'))
		);
    }
}