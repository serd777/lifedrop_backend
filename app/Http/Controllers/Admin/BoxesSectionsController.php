<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Models\BoxSection;
use App\Http\RequestsAnswer;
use App\Http\Services\BoxesSectionsService;
use Illuminate\Http\Request;

/**
 * Boxes Sections Controller
 * @package App\Http\Controllers\Admin
 */
class BoxesSectionsController extends Controller
{
    /**
     * Method, which returns list of sections
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSections()
    {
        return RequestsAnswer::success(BoxesSectionsService::getSections());
    }

    /**
     * Method, which adds section to DB
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addSection(Request $request)
    {
        $validation = \Validator::make($request->all(), [
            'name' => 'required',
        ]);
        if($validation->fails())
            return RequestsAnswer::failed($validation->errors()->first());

        return RequestsAnswer::success(
            BoxesSectionsService::addSection($request->input('name'))
        );
    }

    /**
     * Method, which deletes section from DB
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteSection(Request $request)
    {
        $validation = \Validator::make($request->all(), [
            'id' => 'required|numeric',
        ]);
        if($validation->fails())
            return RequestsAnswer::failed($validation->errors()->first());

        BoxesSectionsService::deleteSection($request->input('id'));
        return RequestsAnswer::success();
    }

    /**
     * Method, which updates section in DB
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateSection(Request $request)
    {
        $validation = \Validator::make($request->all(), [
            'id'   => 'required|numeric',
            'name' => 'required',
        ]);
        if($validation->fails())
            return RequestsAnswer::failed($validation->errors()->first());

        $section = BoxSection::find($request->input('id'));
        if(!$section) RequestsAnswer::failed();

        return RequestsAnswer::success(
            BoxesSectionsService::updateSection(
                $section,
                $request->input('name')
            )
        );
    }
}