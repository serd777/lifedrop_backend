<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\RequestsAnswer;
use App\Http\Services\PromoCodesService;
use Illuminate\Http\Request;

/**
 * Controller for promo codes
 * @package App\Http\Controllers\Admin
 */
class PromoCodesController extends Controller
{
    public function getPromoCodes()
    {
        return RequestsAnswer::success(PromoCodesService::getPromoCodes());
    }

    /**
     * Method, which adds new promo code to DB
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addPromoCode(Request $request)
    {
        $validation = \Validator::make($request->all(), [
            'code'   => 'required',
            'amount' => 'required|numeric',
            'price'  => 'required|numeric',
        ]);
        if($validation->fails())
            return RequestsAnswer::failed($validation->errors()->first());

        return RequestsAnswer::success(
            PromoCodesService::addPromoCode(
                $request->input('code'),
                $request->input('amount'),
                $request->input('price')
            )
        );
    }

    /**
     * Method, which deletes promo codes service
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deletePromoCode(Request $request)
    {
        $validation = \Validator::make($request->all(), [
            'id' => 'required|numeric'
        ]);
        if($validation->fails())
            return RequestsAnswer::failed($validation->errors()->first());

        PromoCodesService::deletePromoCode($request->input('id'));
        return RequestsAnswer::success();
    }
}