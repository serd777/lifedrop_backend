<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Models\Item;
use App\Http\Models\Referral;
use App\Http\Models\User;
use App\Http\Models\UserAddress;
use App\Http\RequestsAnswer;
use App\Http\Services\HistoryService;
use App\Http\Services\UsersService;
use Illuminate\Http\Request;

/**
 * Users Controller
 * @package App\Http\Controllers\Admin
 */
class UsersController extends Controller
{
    /**
     * Method, which returns list of all users
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUsers()
    {
        return RequestsAnswer::success(UsersService::getUsers());
    }

	/**
	 * Method, which returns user's address
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
    public function getUserAddress(Request $request)
    {
    	$validation = \Validator::make($request->all(), [
    		'id' => 'required|numeric',
	    ]);
    	if($validation->fails())
    		return RequestsAnswer::failed($validation->errors()->first());

    	$user = User::find($request->input('id'));
    	$address = UserAddress::where('user_id', $user->id)
	                          ->orderBy('created_at', 'desc')
	                          ->first();
    	if(!$address) $address = new UserAddress(['name' => '']);

    	return RequestsAnswer::success($address);
    }

    /**
     * Method, which returns user info
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserInfo(Request $request)
    {
        $validation = \Validator::make($request->all(), [
            'id' => 'required|numeric'
        ]);
        if ($validation->fails())
            return RequestsAnswer::failed($validation->errors()->first());

        $user_id = $request->input('id');
        if (!User::find($user_id))
            return RequestsAnswer::failed();

        return RequestsAnswer::success(UsersService::getUserInfo($user_id));
    }

	public function getUserStat(Request $request)
	{
		$validation = \Validator::make($request->all(), [
			'id' => 'required|numeric'
		]);
		if ($validation->fails())
			return RequestsAnswer::failed($validation->errors()->first());

		$user_id = $request->input('id');
		if (!User::find($user_id))
			return RequestsAnswer::failed();

		return RequestsAnswer::success([
			'own_box_stat'  => HistoryService::getOwnBox($user_id),
			'payment_stat'  => HistoryService::getPayments($user_id),
			'referral_stat' => HistoryService::getReferralStat($user_id),
			'promo_stat'    => HistoryService::getPromoCode($user_id),
			'drops_stat'    => HistoryService::getDropsStat($user_id),
			'referrals_count' => (int) Referral::ofMentor($user_id)->count()
		]);
	}

    /**
     * Method, which changes user's balance
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changeBalance(Request $request)
    {
        $validation = \Validator::make($request->all(), [
            'id'      => 'required|numeric',
            'balance' => 'required|numeric'
        ]);
        if($validation->fails())
            return RequestsAnswer::failed($validation->errors()->first());

        $user_id = $request->input('id');
        if (!User::find($user_id))
            return RequestsAnswer::failed();

        return RequestsAnswer::success(
            UsersService::updateBalance($user_id, $request->input('balance'))
        );
    }

    /**
     * Method, which updates block of user
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateBlockState(Request $request)
    {
        $validation = \Validator::make($request->all(), [
            'id' => 'required|numeric'
        ]);
        if ($validation->fails())
            return RequestsAnswer::failed($validation->errors()->first());

        $user_id = $request->input('id');
        if (!User::find($user_id))
            return RequestsAnswer::failed();

        return RequestsAnswer::success(UsersService::updateBlockState($user_id));
    }

    /**
     * Method, which updates user's permissions (grant admin priv or reject)
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updatePermissions(Request $request)
    {
        $validation = \Validator::make($request->all(), [
            'id'       => 'required|numeric',
        ]);
        if ($validation->fails())
            return RequestsAnswer::failed($validation->errors()->first());

        $user_id = $request->input('id');
        if (!User::find($user_id))
            return RequestsAnswer::failed();

        return RequestsAnswer::success(UsersService::updatePermissions($user_id));
    }

    /**
     * Method, which returns user's cheats
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCheats(Request $request)
    {
        $validation = \Validator::make($request->all(), [
            'id' => 'required|numeric',
        ]);
        if($validation->fails())
            return RequestsAnswer::failed($validation->errors()->first());

        return RequestsAnswer::success(
            UsersService::getUserCheats($request->input('id'))
        );
    }

    /**
     * Method, which adds user's cheat
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addCheat(Request $request)
    {
        $validation = \Validator::make($request->all(), [
            'user_id' => 'required|numeric',
            'item_id' => 'required|numeric',
            'amount'  => 'required|numeric|min:1',
        ]);
        if($validation->fails())
            return RequestsAnswer::failed($validation->errors()->first());

        $item = Item::find($request->input('item_id'));
        if(!$item) return RequestsAnswer::failed();

        return RequestsAnswer::success(
            UsersService::addUserCheat(
                $request->input('user_id'),
                $item,
                $request->input('amount')
            )
        );
    }

    /**
     * Method, which deletes user's cheat
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteCheat(Request $request)
    {
        $validation = \Validator::make($request->all(), [
            'id' => 'required|numeric',
        ]);
        if($validation->fails())
            return RequestsAnswer::failed($validation->errors()->first());

        UsersService::deleteUserCheat($request->input('id'));
        return RequestsAnswer::success();
    }
}