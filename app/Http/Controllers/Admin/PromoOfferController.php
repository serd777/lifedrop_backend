<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Models\PromoOffer;
use App\Http\RequestsAnswer;
use App\Http\Services\PromoOfferService;
use Illuminate\Http\Request;

/**
 * Promo Offers Controller
 * @package App\Http\Controllers\Admin
 */
class PromoOfferController extends Controller
{
    /**
     * Method, which returns promo offers
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPromoOffers()
    {
        return RequestsAnswer::success(PromoOfferService::getPromoOffers());
    }

    /**
     * Method, which adds new promo offer
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addPromoOffer(Request $request)
    {
        $validation = \Validator::make($request->all(), [
            'title' => 'required',
            'about' => 'required',
            'pos'   => 'required|numeric',
            'img'   => 'required',
        ]);
        if($validation->fails())
            return RequestsAnswer::failed($validation->errors()->first());

        return RequestsAnswer::success(
            PromoOfferService::addPromoOffer(
                $request->input('title'),
                $request->input('about'),
                $request->input('pos'),
                $request->input('img')
            )
        );
    }

    /**
     * Method, which deletes promo offer from DB
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deletePromoOffer(Request $request)
    {
        $validation = \Validator::make($request->all(), [
            'id' => 'required|numeric',
        ]);
        if($validation->fails())
            return RequestsAnswer::failed($validation->errors()->first());

        PromoOfferService::deletePromoOffer($request->input('id'));
        return RequestsAnswer::success();
    }

    /**
     * Method, which updates promo offer text
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updatePromoOfferText(Request $request)
    {
        $validation = \Validator::make($request->all(), [
            'id'     => 'required|numeric',
            'title'  => 'required',
            'about'  => 'required',
	        'pos'    => 'required|numeric',
        ]);
        if($validation->fails())
            return RequestsAnswer::failed($validation->errors()->first());

        $promo = PromoOffer::find($request->input('id'));
        if(!$promo) return RequestsAnswer::failed();

        return RequestsAnswer::success(
            PromoOfferService::updatePromoOffer(
                $promo,
                $request->input('title'),
                $request->input('about'),
                $request->input('pos')
            )
        );
    }

    /**
     * Method, which updates promo offer image
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updatePromoOfferImage(Request $request)
    {
        $validation = \Validator::make($request->all(), [
            'id'  => 'required|numeric',
            'img' => 'required',
        ]);
        if($validation->fails())
            return RequestsAnswer::failed($validation->errors()->first());

        $promo = PromoOffer::find($request->input('id'));
        if(!$promo) return RequestsAnswer::failed();

        return RequestsAnswer::success(
            PromoOfferService::updatePromoOfferImage(
                $promo,
                $request->input('img')
            )
        );
    }
}