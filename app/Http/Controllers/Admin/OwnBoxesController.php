<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Models\Item;
use App\Http\RequestsAnswer;
use App\Http\Services\OwnBoxesService;
use Illuminate\Http\Request;

class OwnBoxesController extends Controller
{
    public function listImages()
    {
        return RequestsAnswer::success(
            OwnBoxesService::getBoxesImages()
        );
    }

    public function listAvailableItems()
    {
    	return RequestsAnswer::success(
    		OwnBoxesService::getAvailableItems()
	    );
    }

    public function addImage(Request $request)
    {
        $validation = \Validator::make($request->all(), [
            'img' => 'required',
        ]);
        if($validation->fails())
            return RequestsAnswer::failed($validation->errors()->first());
        return RequestsAnswer::success(
            OwnBoxesService::addBoxImage($request->input('img'))
        );
    }

    public function deleteImage(Request $request)
    {
        $validation = \Validator::make($request->all(), [
            'id' => 'required|numeric',
        ]);
        if($validation->fails())
            return RequestsAnswer::failed($validation->errors()->first());

        OwnBoxesService::deleteBoxImage($request->input('id'));
        return RequestsAnswer::success();
    }

    public function addAvailableItem(Request $request)
    {
	    $validation = \Validator::make($request->all(), [
		    'id' => 'required|numeric',
	    ]);
	    if($validation->fails())
		    return RequestsAnswer::failed($validation->errors()->first());

	    $item = Item::find($request->input('id'));
	    if(!$item) return RequestsAnswer::failed();

	    return RequestsAnswer::success(
	        OwnBoxesService::addAvailableItem($item)
	    );
    }

    public function deleteAvailableItem(Request $request)
    {
	    $validation = \Validator::make($request->all(), [
		    'id' => 'required|numeric',
	    ]);
	    if($validation->fails())
		    return RequestsAnswer::failed($validation->errors()->first());

	    OwnBoxesService::deleteAvailableItem($request->input('id'));
	    return RequestsAnswer::success();
    }
}