<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Models\Drop;
use App\Http\Models\PayoutComment;
use App\Http\RequestsAnswer;
use App\Http\Services\PayoutsService;
use Illuminate\Http\Request;

/**
 * Controller for actions with payouts
 * @package App\Http\Controllers\Admin
 */
class PayoutsController extends Controller
{
    /**
     * Method, which returns drops with waiting status
     * @return \Illuminate\Http\JsonResponse
     */
    public function getWaitingDrops()
    {
        return RequestsAnswer::success(PayoutsService::getWaitingPayouts());
    }

    /**
     * Method, which returns drops with collecting status
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCollectingDrops()
    {
        return RequestsAnswer::success(PayoutsService::getCollectingPayouts());
    }

    /**
     * Method, which returns drops with sent status
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSentDrops()
    {
        return RequestsAnswer::success(PayoutsService::getSentPayouts());
    }

    /**
     * Method, which marks drop as collecting
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function markAsCollecting(Request $request)
    {
        $validation = \Validator::make($request->all(), [
            'id' => 'required|numeric',
        ]);
        if($validation->fails())
            return RequestsAnswer::failed($validation->errors()->first());

        $drop = Drop::find($request->input('id'));
        if(!$drop) return RequestsAnswer::failed();

        PayoutsService::changeStatus($drop, Drop::STATUS_COLLECTING);
        return RequestsAnswer::success();
    }

    /**
     * Method, which marks drop as sent
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function markAsSent(Request $request)
    {
        $validation = \Validator::make($request->all(), [
            'id'         => 'required|numeric',
            'track_code' => 'required',
        ]);
        if($validation->fails())
            return RequestsAnswer::failed($validation->errors()->first());

        $drop = Drop::find($request->input('id'));
        if(!$drop) return RequestsAnswer::failed();

        $drop->track_code = $request->input('track_code');
        PayoutsService::changeStatus($drop, Drop::STATUS_SENT);

        return RequestsAnswer::success();
    }

    public function getComment(Request $request)
    {
        $validation = \Validator::make($request->all(), [
            'id' => 'required|numeric',
        ]);
        if($validation->fails())
            return RequestsAnswer::failed($validation->errors()->first());

        $item = PayoutComment::ofDrop($request->input('id'))
            ->orderBy('created_at', 'desc')
            ->first();
        (is_null($item)) ? $comment = '' : $comment = $item->comment;

        return RequestsAnswer::success($comment);
    }

    public function addComment(Request $request)
    {
        $validation = \Validator::make($request->all(), [
            'id'        => 'required|numeric',
            'comment'   => 'required',
        ]);
        if($validation->fails())
            return RequestsAnswer::failed($validation->errors()->first());

        PayoutComment::create([
            'user_id' => \Auth::user()->id,
            'drop_id' => $request->input('id'),
            'comment' => $request->input('comment')
        ]);
        return RequestsAnswer::success();
    }
}