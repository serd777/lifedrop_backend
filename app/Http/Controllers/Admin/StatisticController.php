<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Models\User;
use App\Http\RequestsAnswer;
use App\Http\Services\StatisticService;

/**
 * Controller for statistic collection info
 * @package App\Http\Controllers\Admin
 */
class StatisticController extends Controller
{
    /**
     * Method, which returns site stat
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSiteStat()
    {
        return RequestsAnswer::success(
            [
                'today_payments'     => StatisticService::getTodayPaymentsSum(),
                'week_payments'      => StatisticService::getWeekPaymentsSum(),
                'month_payments'     => StatisticService::getMonthPaymentsSum(),

                'today_payouts'      => StatisticService::getTodayPayoutsSum(),
                'week_payouts'       => StatisticService::getWeekPayoutsSum(),
                'month_payouts'      => StatisticService::getMonthPayoutsSum(),

                'total_users'        => User::count(),
                'games_played_today' => StatisticService::getGamesPlayedToday(),

	            'donates_by_day'     => StatisticService::getDonateGroupByDays(),
	            'payouts_by_day'     => StatisticService::getPayoutsGroupByDays()
            ]
        );
    }
}