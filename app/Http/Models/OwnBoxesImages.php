<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OwnBoxesImages extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'img_path',
    ];

    protected $table = 'own_boxes_images';
    protected $dates = ['deleted_at'];
}