<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * User Model
 * @package App\Http\Models
 */
class User extends Authenticatable
{
    use Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'avatar', 'vk_id',
        'balance',
        'is_blocked', 'is_admin',
    ];

    /**
     * Table Name
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
    ];

    /**
     * Dates array
     *
     * @var array
     */
    protected $dates = [
        'deleted_at',
    ];

    /**
     * Scope of VK Id
     *
     * @param $query
     * @param $vk_id
     * @return mixed
     */
    public function scopeOfVk($query, $vk_id)
    {
        return $query->where('vk_id', $vk_id);
    }
}
