<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Drop extends Model
{
    // Статус ожидания решения пользователя
    const STATUS_UNCHECKED = 'Unchecked';
    // Статус проданного предмета
    const STATUS_SOLD = 'Sold';
    // Статус ожидания проверки
    const STATUS_WAITING = 'Waiting';
    // Статус ожидания отправки
    const STATUS_COLLECTING = 'Collecting';
    // Статус завершенной отправки
    const STATUS_SENT = 'Sent';

    protected $fillable = [
        'item_id', 'user_id', 'status', 'track_code', 'border_color'
    ];

    protected $table = 'drops';

    public function item()
    {
        return $this->belongsTo('App\Http\Models\Item', 'item_id')->withTrashed();
    }
    public function user()
    {
        return $this->belongsTo('App\Http\Models\User', 'user_id')->withTrashed();
    }

    public function scopeOfRequested($query)
    {
    	return $query->where('status', '!=', self::STATUS_UNCHECKED)
	                 ->where('status', '!=', self::STATUS_SOLD);
    }
    public function scopeOfWaiting($query)
    {
        return $query->where('status', self::STATUS_WAITING);
    }
    public function scopeOfCollecting($query)
    {
        return $query->where('status', self::STATUS_COLLECTING);
    }
    public function scopeOfSent($query)
    {
        return $query->where('status', self::STATUS_SENT);
    }
    public function scopeOfUser($query, $user_id)
    {
        return $query->where('user_id', $user_id);
    }
}