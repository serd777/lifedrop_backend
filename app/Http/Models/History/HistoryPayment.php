<?php namespace App\Http\Models\History;

use Illuminate\Database\Eloquent\Model;

class HistoryPayment extends Model
{
	protected $fillable = [
		'user_id', 'amount',
	];

	protected $table = 'history_payment';

	public function user()
	{
		return $this->belongsTo('App\Http\Models\User', 'user_id');
	}
	public function scopeOfUser($query, $user_id)
	{
		return $query->where('user_id', $user_id);
	}
}