<?php namespace App\Http\Models\History;

use Illuminate\Database\Eloquent\Model;

class HistoryOwnBox extends Model
{
	protected $fillable = [
		'mentor_id', 'user_id', 'box_id', 'amount'
	];

	protected $table = 'history_own_boxes';

	public function mentor()
	{
		return $this->belongsTo('App\Http\Models\User', 'mentor_id');
	}
	public function user()
	{
		return $this->belongsTo('App\Http\Models\User', 'user_id');
	}
	public function box()
	{
		return $this->belongsTo('App\Http\Models\OwnBox', 'box_id');
	}

	public function scopeOfMentor($query, $mentor_id)
	{
		return $query->where('mentor_id', $mentor_id);
	}
}