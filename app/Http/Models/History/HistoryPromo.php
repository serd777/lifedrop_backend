<?php namespace App\Http\Models\History;

use Illuminate\Database\Eloquent\Model;

class HistoryPromo extends Model
{
	protected $fillable = [
		'user_id', 'code_id',
	];

	protected $table = 'history_promo';

	public function user()
	{
		return $this->belongsTo('App\Http\Models\User', 'user_id');
	}
	public function code()
	{
		return $this->belongsTo('App\Http\Models\PromoCode', 'code_id')->withTrashed();
	}

	public function scopeOfUser($query, $user_id)
	{
		return $query->where('user_id', $user_id);
	}
}