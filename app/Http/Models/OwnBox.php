<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OwnBox extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name', 'price', 'img_id', 'user_id',
    ];

    protected $table = 'own_boxes';
    protected $dates = ['deleted_at'];

    public function img()
    {
    	return $this->belongsTo('App\Http\Models\OwnBoxesImages', 'img_id');
    }

    public function scopeOfUser($query, $user_id)
    {
    	return $query->where('user_id', $user_id);
    }
}