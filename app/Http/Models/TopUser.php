<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class TopUser extends Model
{
	protected $fillable = [
		'user_id', 'games', 'profit',
	];

	protected $table = 'top_users';

	public function user()
	{
		return $this->belongsTo('App\Http\Models\User', 'user_id');
	}
}