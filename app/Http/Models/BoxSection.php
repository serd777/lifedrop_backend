<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BoxSection extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name'
    ];

    protected $table = 'boxes_sections';
    protected $dates = ['deleted_at'];
}