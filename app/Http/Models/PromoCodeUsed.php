<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class PromoCodeUsed extends Model
{
    protected $fillable = [
        'user_id', 'code_id',
    ];

    protected $table = 'promo_codes_used';

    public function user()
    {
        return $this->belongsTo('App\Http\Models\User', 'user_id');
    }
    public function code()
    {
        return $this->belongsTo('App\Http\Models\PromoCode', 'code_id');
    }
}