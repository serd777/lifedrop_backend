<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Referral extends Model
{
    protected $fillable = [
        'mentor_id', 'user_id',
    ];

    protected $table = 'referrals';

    public function mentor()
    {
        return $this->belongsTo('App\Http\Models\User', 'mentor_id');
    }
    public function user()
    {
        return $this->belongsTo('App\Http\Models\User', 'user_id');
    }

    public function scopeOfMentor($query, $mentor_id)
    {
    	return $query->where('mentor_id', $mentor_id);
    }
}