<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class UserAddress extends Model
{
    protected $fillable = [
        'user_id',
        'name', 'country', 'region', 'city', 'address',
        'post_index', 'has_post_index',
        'phone', 'additional_info'
    ];

    protected $table = 'users_addresses';

    public function user()
    {
        return $this->belongsTo('App\Http\Models\User', 'user_id');
    }
}