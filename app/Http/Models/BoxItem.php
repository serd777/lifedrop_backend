<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class BoxItem extends Model
{
    protected $fillable = [
        'box_id', 'item_id', 'percent'
    ];

    protected $table = 'boxes_items';

    public function box()
    {
        return $this->belongsTo('App\Http\Models\Box', 'box_id');
    }
    public function item()
    {
        return $this->belongsTo('App\Http\Models\Item', 'item_id');
    }
    public function scopeOfBox($query, $box_id)
    {
        return $query->where('box_id', $box_id);
    }
    public function scopeOfItem($query, $item_id)
    {
        return $query->where('item_id', $item_id);
    }
}