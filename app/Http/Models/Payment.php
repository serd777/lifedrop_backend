<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = [
        'user_id', 'amount', 'type', 'payed', 'remote_id',
    ];

    protected $table = 'payments';

    public function scopeOfFreeKassa($query)
    {
        return $query->where('type', 'free-kassa');
    }
    public function scopeOfSkinPay($query)
    {
        return $query->where('type', 'skinpay');
    }
    public function scopeOfPayed($query)
    {
        return $query->where('payed', true);
    }
    public function scopeOfUser($query, $user_id)
    {
    	return $query->where('user_id', $user_id);
    }
}