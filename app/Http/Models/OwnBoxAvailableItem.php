<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class OwnBoxAvailableItem extends Model
{
	protected $fillable = [
		'item_id',
	];

	protected $table = 'own_boxes_items_available';

	public function item()
	{
		return $this->belongsTo('App\Http\Models\Item', 'item_id');
	}
	public function scopeOfItem($query, $item_id)
    {
        return $query->where('item_id', $item_id);
    }
}