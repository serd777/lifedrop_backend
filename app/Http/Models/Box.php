<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Box extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
	    'img_path',
	    'price',
	    'is_enabled',
	    'section_id',
	    'color_num',
	    'pos',
	    'border_color',
	    'is_priority',
    ];

    protected $table = 'boxes';
    protected $dates = ['deleted_at'];

    public function section()
    {
        return $this->belongsTo('App\Http\Models\BoxSection', 'section_id');
    }
    public function scopeOfActive($query)
    {
    	return $query->where('is_enabled', true);
    }
}