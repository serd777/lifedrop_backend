<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    protected $fillable = [
       'item_id', 'box_id', 'user_id', 'own_box_id'
    ];

    protected $table = 'games';

    public function item()
    {
        return $this->belongsTo('App\Http\Models\Item', 'item_id')->withTrashed();
    }
    public function box()
    {
        return $this->belongsTo('App\Http\Models\Box', 'box_id')->withTrashed();
    }
    public function user()
    {
        return $this->belongsTo('App\Http\Models\User', 'user_id');
    }

    public function scopeOfUser($query, $user_id)
    {
        return $query->where('user_id', $user_id);
    }

    public function scopeOfOwnBox($query, $box_id)
    {
    	return $query->where('own_box_id', $box_id);
    }
}