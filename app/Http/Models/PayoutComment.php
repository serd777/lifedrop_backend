<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class PayoutComment extends Model
{
    protected $fillable = [
        'comment', 'user_id', 'drop_id',
    ];

    protected $table = 'payout_comments';

    public function scopeOfDrop($query, $drop_id)
    {
        return $query->where('drop_id', $drop_id);
    }
}