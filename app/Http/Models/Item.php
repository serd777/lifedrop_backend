<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Item extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name', 'img_path', 'price'
    ];

    protected $table = 'items';
    protected $dates = ['deleted_at'];
}