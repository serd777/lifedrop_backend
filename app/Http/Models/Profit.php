<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Profit extends Model
{
    protected $fillable = [
        'user_id', 'item_id', 'profit',
    ];

    protected $table = 'profits';

    public function user()
    {
        return $this->belongsTo('App\Http\Models\User', 'user_id');
    }
    public function item()
    {
        return $this->belongsTo('App\Http\Models\Item', 'item_id');
    }

    public function scopeOfUser($query, $user_id)
    {
        return $query->where('user_id', $user_id);
    }
}