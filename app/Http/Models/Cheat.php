<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cheat extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'user_id', 'item_id', 'amount'
    ];

    protected $table = 'cheats';
    protected $dates = ['deleted_at'];

    public function user()
    {
        return $this->belongsTo('App\Http\Models\User', 'user_id');
    }
    public function item()
    {
        return $this->belongsTo('App\Http\Models\Item', 'item_id');
    }

    public function scopeOfUser($query, $user_id)
    {
        return $query->where('user_id', $user_id);
    }
}