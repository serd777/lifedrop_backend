<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PromoCode extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'code', 'amount', 'price'
    ];

    protected $table = 'promo_codes';
    protected $dates = ['deleted_at'];

}