<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PromoOffer extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'title', 'about', 'img_path', 'pos',
    ];

    protected $table = 'promo_offers';
    protected $dates = ['deleted_at'];
}