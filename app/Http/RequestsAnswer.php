<?php namespace App\Http;

/**
 * Method, which generates request answers
 * @package App\Http
 */
class RequestsAnswer
{
    /**
     * Success answer
     */
    const SUCCESS = true;
    /**
     * Failed answer
     */
    const FAILED  = false;

    /**
     * Method, which returns success answer
     * @param null $data
     * @return \Illuminate\Http\JsonResponse
     */
    public static function success($data = null)
    {
        return self::generateAnswer(self::SUCCESS, $data);
    }

    /**
     * Method, which returns failed answer
     * @param null $data
     * @return \Illuminate\Http\JsonResponse
     */
    public static function failed($data = null)
    {
        return self::generateAnswer(self::FAILED, $data);
    }

    /**
     * Method, which generates answer
     * @param $status
     * @param $data
     * @return \Illuminate\Http\JsonResponse
     */
    private static function generateAnswer($status, $data)
    {
        return \Response::json([
            'status' => $status,
            'data'   => $data,
        ]);
    }
}