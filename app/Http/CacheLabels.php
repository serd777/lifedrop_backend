<?php namespace App\Http;

/**
 * Labels for cache access
 * @package App\Http
 */
class CacheLabels
{
    //  List of registered users
    const UsersList = 'users_list';
    //  List of game items
    const ItemsList = 'items_list';
    //  List of game cases
    const BoxesList = 'boxes_list';
    //  List of active cases
	const BoxesActiveList = 'boxes_active_list';
    //  List of sections for cases
    const BoxesSectionsList = 'boxes_sections_list';
    //  List of boxes items
    const BoxesItems = 'boxes_items_';
    //  List of promo codes
    const PromoCodesList = 'promo_codes_list';
    //  List of sent drops
    const SentDropsList = 'sent_drops_list';
    //  List of promo offers
    const PromoOffersList = 'promo_offers_list';
    //  List of user's drops
    const UserDropsList = 'user_drops_list_';
    //  Amount of games
    const UserGamesAmount = 'user_games_amount_';
    //  Amount of profit
    const UserProfitAmount = 'user_profit_amount_';
    //  List of own box images
	const OwnBoxImagesList = 'own_box_images_list';
	//  List of own boxes
	const OwnBoxesList = 'own_boxes_list';
	//  List of own box items
	const OwnBoxItems = 'own_box_items_';
	//  List of top users
	const TopUsersList = 'top_users_list';
	//  List of available items for own boxes
	const OwnBoxAvailableItems = 'own_boxes_avail_items';
	//  List of own box cache
	const OwnBoxOpenedCache = 'own_boxes_opened';
	//  Referrals amount
	const ReferralsAmount = 'referrals_amount_';
}