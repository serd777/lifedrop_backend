<?php namespace App\Http\Services;

use App\Http\CacheLabels;
use App\Http\Models\BoxSection;

/**
 * Boxes Sections Service
 * @package App\Http\Services
 */
class BoxesSectionsService
{
    /**
     * Method, which returns list of sections
     * @return \Illuminate\Database\Eloquent\Collection|mixed|static[]
     */
    public static function getSections()
    {
        if(\Cache::has(CacheLabels::BoxesSectionsList))
            return \Cache::get(CacheLabels::BoxesSectionsList);
        return self::updateSectionsCache();
    }

    /**
     * Method, which creates new boxes section
     * @param $name
     * @return mixed
     */
    public static function addSection($name)
    {
        $section = BoxSection::create([
            'name' => $name
        ]);

        self::updateSectionsCache();
        return $section;
    }

    /**
     * Method, which updates section
     * @param $section
     * @param $name
     * @return mixed
     */
    public static function updateSection($section, $name)
    {
        $section->name = $name;
        $section->save();

        self::updateSectionsCache();
        return $section;
    }

    /**
     * Method, which deletes section from DB
     * @param $id
     */
    public static function deleteSection($id)
    {
        BoxSection::where('id', $id)->delete();
        self::updateSectionsCache();
    }

    /**
     * Method, which updates sections cache
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function updateSectionsCache()
    {
        $sections = BoxSection::all();
        \Cache::put(CacheLabels::BoxesSectionsList, $sections, 30);

        return $sections;
    }
}