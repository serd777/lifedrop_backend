<?php namespace App\Http\Services;

use App\Http\CacheLabels;
use App\Http\Models\Game;
use App\Http\Models\OwnBox;
use App\Http\Models\OwnBoxAvailableItem;
use App\Http\Models\OwnBoxesImages;
use App\Http\Models\OwnBoxItem;
use Carbon\Carbon;

/**
 * Own Box Service
 * @package App\Http\Services
 */
class OwnBoxesService
{
	/**
	 * Method, which returns box'es images
	 * @return \Illuminate\Database\Eloquent\Collection|mixed|static[]
	 */
	public static function getBoxesImages()
	{
		if(\Cache::has(CacheLabels::OwnBoxImagesList))
			return \Cache::get(CacheLabels::OwnBoxImagesList);
		return self::updateBoxesImagesCache();
	}

	/**
	 * Method, which returns available items
	 * @return \Illuminate\Database\Eloquent\Collection|mixed|static[]
	 */
	public static function getAvailableItems()
	{
		if(\Cache::has(CacheLabels::OwnBoxAvailableItems))
			return \Cache::get(CacheLabels::OwnBoxAvailableItems);
		return self::updateAvailableItemsCache();
	}

	/**
	 * Method, which returns user's boxes
	 * @return \Illuminate\Database\Eloquent\Collection|mixed|static[]
	 */
	public static function getOwnBoxes()
	{
		if(\Cache::has(CacheLabels::OwnBoxesList))
			return \Cache::get(CacheLabels::OwnBoxesList);
		return self::updateBoxesCache();
	}

	/**
	 * Method, which returns list of box items
	 * @param $box_id
	 *
	 * @return mixed
	 */
	public static function getOwnBoxesItems($box_id)
	{
		if(\Cache::has(CacheLabels::OwnBoxItems . $box_id))
			return \Cache::get(CacheLabels::OwnBoxItems . $box_id);
		return self::updateBoxItemsCache($box_id);
	}

	/**
	 * Method, which returns own boxes stat
	 * @return array|mixed
	 */
	public static function getOwnBoxesStat()
	{
		if(\Cache::has(CacheLabels::OwnBoxOpenedCache))
			return \Cache::get(CacheLabels::OwnBoxOpenedCache);
		return self::updateOwnBoxesOpenedCache();
	}

	/**
	 * Method, which returns how many boxes user have created today
	 * @param $user_id
	 *
	 * @return mixed
	 */
	public static function getUserBoxesTodayAmount($user_id)
	{
		$time = Carbon::now()->startOfDay();
		return OwnBox::ofUser($user_id)->where('created_at', '>=', $time)->count();
	}

	/**
	 * Method, which creates own box
	 * @param $user_id
	 * @param $name
	 * @param $items
	 * @param $img_id
	 *
	 * @return mixed
	 */
	public static function createOwnBox($user_id, $name, $items, $img_id)
	{
		$price = self::calcOwnBoxPrice($items);
		$box = OwnBox::create([
			'name'    => $name,
			'price'   => $price,
			'img_id'  => $img_id,
			'user_id' => $user_id,
		]);

		foreach($items as $item)
			OwnBoxItem::create([
				'box_id'  => $box->id,
				'item_id' => $item->id,
			]);

		self::updateBoxesCache();
		self::updateBoxItemsCache($box->id);
		$box->img = OwnBoxesImages::find($img_id);
		return $box;
	}

	private static function calcOwnBoxPrice($items) {
	    $average = 0;
	    foreach($items as $item)
	        $average += $item->price;
	    $average = round($average / $items->count());

	    $price = round($average / 10);

	    $min_item = self::findMinItem($items);
	    if($price < $min_item->price)
	        $price = $min_item->price + round($min_item->price * 0.5);

	    return $price;
    }

    private static function findMinItem($items) {
	    $min = $items->first();

	    foreach($items as $item)
	        if($item->price < $min->price)
	            $min = $item;

	    return $min;
    }

    /**
     * Method, which adds new own box image
     * @param $img
     * @return mixed
     */
	public static function addBoxImage($img)
	{
        $box_img = OwnBoxesImages::create([]);
        $file_path = 'images/own_boxes/' . md5($box_img->id . '-box') . '.png';

        $file = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $img));
        \File::put($file_path, $file);

        $box_img->img_path = '/' . $file_path;
        $box_img->save();

        self::updateBoxesImagesCache();
        return $box_img;
	}

    /**
     * Method, which deletes own box image
     * @param $id
     */
	public static function deleteBoxImage($id)
    {
        OwnBoxesImages::where('id', $id)->delete();
        self::updateBoxesImagesCache();
    }

	/**
	 * Method, which adds available item
	 * @param $item
	 *
	 * @return mixed
	 */
    public static function addAvailableItem($item)
    {
    	$a_item = OwnBoxAvailableItem::create([
    		'item_id' => $item->id,
	    ]);
    	self::updateAvailableItemsCache();

	    $a_item->item = $item;
    	return $a_item;
    }

	/**
	 * Method, which updates available item cache
	 * @param $id
	 */
    public static function deleteAvailableItem($id)
    {
    	OwnBoxAvailableItem::where('id', $id)->delete();
    	self::updateAvailableItemsCache();
    }

    /**
     * Method, which updates boxes images cache
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
	public static function updateBoxesImagesCache()
	{
		$images = OwnBoxesImages::all();
		\Cache::put(CacheLabels::OwnBoxImagesList, $images, 60);
		return $images;
	}

    /**
     * Method, which updates boxes cache
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
	public static function updateBoxesCache()
	{
		$boxes = OwnBox::with('img')->get();
		\Cache::put(CacheLabels::OwnBoxesList, $boxes, 60);
		return $boxes;
	}

    /**
     * Method, which updates box items cache
     * @param $box_id
     * @return mixed
     */
	public static function updateBoxItemsCache($box_id)
	{
        $box_items = OwnBoxItem::ofBox($box_id)->with('item')->get();
        \Cache::put(CacheLabels::OwnBoxItems . $box_id, $box_items, 30);
        return $box_items;
	}

	/**
	 * Method, which updates boxes' items cache
	 */
	public static function updateBoxesItemsCache()
	{
		$boxes = self::getOwnBoxes();
		foreach ($boxes as $box)
			self::updateBoxItemsCache($box->id);
	}

	/**
	 * Method, which updates available items cache
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	public static function updateAvailableItemsCache()
	{
		$items = OwnBoxAvailableItem::with('item')->get();
		\Cache::put(CacheLabels::OwnBoxAvailableItems, $items, 120);
		return $items;
	}

	/**
	 * Method, which updates cache for own boxes stat
	 * @return array
	 */
	public static function updateOwnBoxesOpenedCache()
	{
		$boxes = self::getOwnBoxes();
		$result = [];

		foreach($boxes as $box) {
			$amount = Game::ofOwnBox($box->id)->count();
			$result[] = collect([
				'box_id' => $box->id,
				'amount' => $amount
			]);
		}

		\Cache::put(CacheLabels::OwnBoxOpenedCache, $result, 120);
		return $result;
	}
}