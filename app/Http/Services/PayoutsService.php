<?php namespace App\Http\Services;

use App\Http\CacheLabels;
use App\Http\Models\Drop;

/**
 * Service for payouts
 * @package App\Http\Services
 */
class PayoutsService
{
    /**
     * Method, which returns payouts with status "Waiting"
     * @return mixed
     */
    public static function getWaitingPayouts()
    {
        return Drop::ofWaiting()
            ->with('item')
            ->with('user')
            ->orderBy('updated_at', 'asc')
            ->get();
    }

    /**
     * Method, which returns payouts with status "Collecting"
     * @return mixed
     */
    public static function getCollectingPayouts()
    {
        return Drop::ofCollecting()
            ->with('item')
            ->with('user')
            ->orderBy('updated_at', 'asc')
            ->get();
    }

    /**
     * Method, which returns payouts with status "Sent"
     * @return mixed
     */
    public static function getSentPayouts()
    {
        if(\Cache::has(CacheLabels::SentDropsList))
            return \Cache::get(CacheLabels::SentDropsList);
        return self::updateSentPayoutsCache();
    }

    /**
     * Method, which changes drop status
     * @param $drop
     * @param $status
     */
    public static function changeStatus($drop, $status)
    {
        $drop->status = $status;
        $drop->save();

        if($status == Drop::STATUS_SENT)
            self::updateSentPayoutsCache();
        else if($status == Drop::STATUS_SENT)
	        UsersService::updateUserProfitCache($drop->user_id);

        DropsService::updateDropsCache($drop->user_id);
    }

    /**
     * Method, which updates cache for sent payouts
     * @return mixed
     */
    public static function updateSentPayoutsCache()
    {
        $drops = Drop::ofSent()
            ->with('item')
            ->with('user')
            ->orderBy('updated_at', 'asc')
            ->get();
        \Cache::put(CacheLabels::SentDropsList, $drops, 60);

        return $drops;
    }
}