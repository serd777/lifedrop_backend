<?php namespace App\Http\Services;

use App\Http\CacheLabels;
use App\Http\Models\Box;
use App\Http\Models\BoxItem;
use App\Http\Models\BoxSection;
use App\Http\Models\Item;

/**
 * Prototype for new case
 * @package App\Http\Services
 */
class NewBox
{
    /**
     * @var string
     */
    var $name;
    /**
     * @var int
     */
    var $price;
    /**
     * @var BoxSection
     */
    var $section;
    /**
     * @var string
     */
    var $img;
	/**
	 * @var int
	 */
    var $color_num;
    /**
     * @var int
     */
    var $pos;
	/**
	 * @var string
	 */
    var $border_color;
	/**
	 * @var bool
	 */
    var $is_priority;
}

/**
 * Service for game boxes
 * @package App\Http\Services
 */
class BoxesService
{
    /**
     * Method, which returns games cases
     * @return \Illuminate\Database\Eloquent\Collection|mixed|static[]
     */
    public static function getBoxes()
    {
        if(\Cache::has(CacheLabels::BoxesList))
            return \Cache::get(CacheLabels::BoxesList);
        return self::updateBoxesCache();
    }

	/**
	 * Method, which returns active boxes
	 * @return mixed
	 */
    public static function getActiveBoxes()
    {
	    if(\Cache::has(CacheLabels::BoxesActiveList))
		    return \Cache::get(CacheLabels::BoxesActiveList);
	    return self::updateBoxesActiveCache();
    }

    /**
     * Method, which returns box'es items
     * @param $box_id
     * @return mixed
     */
    public static function getBoxItems($box_id)
    {
        if(\Cache::has(CacheLabels::BoxesItems . $box_id))
            return \Cache::get(CacheLabels::BoxesItems . $box_id);
        return self::updateBoxItemsCache($box_id);
    }

    /**
     * @param NewBox $new_box
     * @return Box
     */
    public static function addBox($new_box)
    {
        $box = Box::create([
            'name'          => $new_box->name,
            'price'         => $new_box->price,
            'color_num'     => $new_box->color_num,
            'pos'           => $new_box->pos,
            'section_id'    => $new_box->section->id,
	        'border_color'  => $new_box->border_color,
	        'is_priority'   => $new_box->is_priority,
        ]);

        $file_path = 'images/boxes/' . md5($box->id . '-box') . '.png';

        $file = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $new_box->img));
        \File::put($file_path, $file);

        $box->img_path = '/' . $file_path;
        $box->save();
	    $box->section = $new_box->section;

        self::updateBoxesCache();
        return $box;
    }

	/**
	 * Method, which updates selected box
	 *
	 * @param Box $box
	 * @param string $name
	 * @param int $price
	 * @param bool $is_enabled
	 * @param int $color_num
	 * @param int $pos
	 * @param BoxSection $section
	 * @param string $border_color
	 * @param bool $is_priority
	 *
	 * @return Box
	 */
    public static function updateBox(
    	$box,
	    $name,
	    $price,
	    $is_enabled,
	    $color_num,
	    $pos,
	    $section,
	    $border_color,
		$is_priority
    ) {
        $box->name = $name;
        $box->price = $price;
        $box->is_enabled = $is_enabled;
        $box->color_num = $color_num;
        $box->pos = $pos;
        $box->section_id = $section->id;
	    $box->border_color = $border_color;
	    $box->is_priority = $is_priority;
        $box->save();

	    $box->section = $section;

        self::updateBoxesCache();
        return $box;
    }

	/**
	 * Method, which updates box's img
	 * @param $box
	 * @param $img
	 *
	 * @return mixed
	 */
    public static function updateBoxImg($box, $img)
    {
	    $time = \Carbon\Carbon::now()->toDateTimeString();
	    $file_path = 'images/boxes/' . md5($box->id . '-box-' . $time) . '.png';

	    $file = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $img));
	    \File::put($file_path, $file);

	    $box->img_path = '/' . $file_path;
	    $box->save();

	    self::updateBoxesCache();
	    return $box;
    }

    /**
     * Method, which deletes box from DB
     * @param $box_id
     */
    public static function deleteBox($box_id)
    {
        Box::where('id', $box_id)->delete();
        self::updateBoxesCache();
    }

    /**
     * @param Box $box
     * @param Item $item
     * @param int $percent
     * @return mixed
     */
    public static function addItemToBox($box, $item, $percent)
    {
        $box_item = BoxItem::create([
            'box_id'  => $box->id,
            'item_id' => $item->id,
            'percent' => $percent
        ]);
        $box_item->box = $box;
        $box_item->item = $item;

        self::updateBoxItemsCache($box->id);
        return $box_item;
    }

    /**
     * @param BoxItem $box_item
     * @throws \Exception
     */
    public static function deleteItemFromBox($box_item)
    {
        $box_item->delete();
        self::updateBoxItemsCache($box_item->box_id);
    }

    /**
     * Method, which updates drop percent
     * @param BoxItem $box_item
     * @param int $percent
     * @return BoxItem
     */
    public static function updateItemPercent($box_item, $percent)
    {
        $box_item->percent = $percent;
        $box_item->save();

        self::updateBoxItemsCache($box_item->box_id);
        return $box_item;
    }

    /**
     * Method, which updates boxes cache
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function updateBoxesCache()
    {
        $boxes = Box::with('section')->get();
        \Cache::put(CacheLabels::BoxesList, $boxes, 30);

        self::updateBoxesActiveCache();
        return $boxes;
    }

	/**
	 * Method, which updated boxes active cache
	 * @return mixed
	 */
    public static function updateBoxesActiveCache()
    {
	    $active_boxes = Box::ofActive()->orderBy('pos', 'asc')->get();
	    \Cache::put(CacheLabels::BoxesActiveList, $active_boxes, 30);

	    return $active_boxes;
    }

    /**
     * Method, which updates cache for box items
     * @param $box_id
     * @return mixed
     */
    public static function updateBoxItemsCache($box_id)
    {
        $items = BoxItem::ofBox($box_id)
            ->with('item')
	        ->with('box')
            ->get();
        \Cache::put(CacheLabels::BoxesItems . $box_id, $items, 30);

        return $items;
    }

    /**
     * Method, which updates cache for all boxes items
     */
    public static function updateBoxesItemsCache()
    {
        $boxes = self::getBoxes();
        foreach($boxes as $box)
            self::updateBoxItemsCache($box->id);
    }
}