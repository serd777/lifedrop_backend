<?php namespace App\Http\Services;

use App\Http\CacheLabels;
use App\Http\Models\History\HistoryPromo;
use App\Http\Models\PromoCode;
use App\Http\Models\PromoCodeUsed;
use App\Http\Models\User;

/**
 * Promo Codes Service
 * @package App\Http\Services
 */
class PromoCodesService
{
    /**
     * Method, which returns promo codes list
     * @return \Illuminate\Database\Eloquent\Collection|mixed|static[]
     */
    public static function getPromoCodes()
    {
        if(\Cache::has(CacheLabels::PromoCodesList))
            return \Cache::get(CacheLabels::PromoCodesList);
        return self::updatePromoCodesCache();
    }

    /**
     * Method, which adds promo code to DB
     * @param string $code
     * @param int $amount
     * @param int $price
     * @return mixed
     */
    public static function addPromoCode($code, $amount, $price)
    {
        $code = PromoCode::create([
            'code'   => $code,
            'amount' => $amount,
            'price'  => $price
        ]);

        self::updatePromoCodesCache();
        return $code;
    }

    /**
     * Method, which deletes promo from DB
     * @param int $id
     */
    public static function deletePromoCode($id)
    {
        PromoCode::where('id', $id)->delete();
        self::updatePromoCodesCache();
    }

    /**
     * Method, which activates promo code for user
     * @param User $user
     * @param PromoCode $promo
     * @return int
     * @throws \Exception
     */
    public static function activatePromo($user, $promo)
    {
        PromoCodeUsed::create([
            'user_id' => $user->id,
            'code_id' => $promo->id,
        ]);

        $promo->amount--;
        if(!$promo->amount)
            $promo->delete();
        else $promo->save();

        $user->balance += $promo->price;
        $user->save();

        HistoryPromo::create([
        	'user_id' => $user->id,
	        'code_id' => $promo->id
        ]);

        self::updatePromoCodesCache();
        return $promo->price;
    }

    /**
     * Method, which checks
     * @param User $user
     * @param PromoCode $promo
     * @return bool
     */
    public static function isAlreadyActivated($user, $promo)
    {
        return PromoCodeUsed::where('code_id', $promo->id)
            ->where('user_id', $user->id)
            ->first() != null;
    }

    /**
     * Method, which updates promo codes cache
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function updatePromoCodesCache()
    {
        $promo_codes = PromoCode::all();
        \Cache::put(CacheLabels::PromoCodesList, $promo_codes, 30);

        return $promo_codes;
    }
}