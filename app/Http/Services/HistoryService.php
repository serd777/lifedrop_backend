<?php namespace App\Http\Services;

use App\Http\Models\History\HistoryOwnBox;
use App\Http\Models\History\HistoryPayment;
use App\Http\Models\History\HistoryPromo;
use App\Http\Models\History\HistoryReferrals;

class HistoryService
{
	public static function getPayments($user_id)
	{
		return HistoryPayment::ofUser($user_id)
		                     ->orderBy('created_at', 'desc')
		                     ->get();
	}

	public static function getPromoCode($user_id)
	{
		return HistoryPromo::ofUser($user_id)
		                   ->with('code')
		                   ->orderBy('created_at', 'desc')
		                   ->get();
	}

	public static function getReferralStat($user_id)
	{
		return HistoryReferrals::ofMentor($user_id)
		                       ->with('user')
		                       ->orderBy('created_at', 'desc')
		                       ->get();
	}

	public static function getOwnBox($user_id)
	{
		return HistoryOwnBox::ofMentor($user_id)
		                    ->with('box')
		                    ->orderBy('created_at', 'desc')
		                    ->get();
	}

	public static function getDropsStat($user_id)
	{
		return DropsService::getDrops($user_id);
	}
}