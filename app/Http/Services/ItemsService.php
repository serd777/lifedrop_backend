<?php namespace App\Http\Services;

use App\Http\CacheLabels;
use App\Http\Models\BoxItem;
use App\Http\Models\Item;
use App\Http\Models\OwnBoxAvailableItem;
use App\Http\Models\OwnBoxItem;

/**
 * Items Service Class
 * @package App\Http\Services
 */
class ItemsService
{
    /**
     * Method, which returns items list
     * @return \Illuminate\Database\Eloquent\Collection|mixed|static[]
     */
    public static function getItems()
    {
        if(\Session::has(CacheLabels::ItemsList))
            return \Session::get(CacheLabels::ItemsList);
        return self::updateItemsCache();
    }

    /**
     * Method, which adds new item to DB
     * @param $name
     * @param $img
     * @param $price
     * @return mixed
     */
    public static function addItem($name, $img, $price)
    {
        $item = Item::create([
            'name'  => $name,
            'price' => $price
        ]);

        $file_path = 'images/items/' . md5($item->id . '-item') . '.png';

        $file = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $img));
        \File::put($file_path, $file);

        $item->img_path = '/' . $file_path;
        $item->save();

        self::updateItemsCache();
        return $item;
    }

    /**
     * Method, which deletes item from scope
     * @param $item_id
     */
    public static function deleteItem($item_id)
    {
        Item::where('id', $item_id)->delete();
        BoxItem::ofItem($item_id)->delete();
        OwnBoxItem::ofItem($item_id)->delete();
        OwnBoxAvailableItem::ofItem($item_id)->delete();

        self::updateItemsCache();
        BoxesService::updateBoxesItemsCache();
	    OwnBoxesService::updateBoxesItemsCache();
	    OwnBoxesService::updateAvailableItemsCache();
    }

    /**
     * Method, which updates item info
     * @param $item
     * @param $name
     * @param $price
     * @return mixed
     */
    public static function updateItem($item, $name, $price)
    {
        $item->name = $name;
        $item->price = $price;
        $item->save();

        self::updateItemsCache();
        BoxesService::updateBoxesItemsCache();
        OwnBoxesService::updateBoxesItemsCache();
	    OwnBoxesService::updateAvailableItemsCache();
        return $item;
    }

	/**
	 * Method, which updates item's img
	 * @param $item
	 * @param $img
	 *
	 * @return mixed
	 */
    public static function updateItemImg($item, $img)
    {
	    $time = \Carbon\Carbon::now()->toDateTimeString();
	    $file_path = 'images/items/' . md5($item->id . '-item-' . $time) . '.png';

	    $file = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $img));
	    \File::put($file_path, $file);

	    $item->img_path = '/' . $file_path;
	    $item->save();

	    self::updateItemsCache();
	    BoxesService::updateBoxesItemsCache();
	    OwnBoxesService::updateBoxesItemsCache();
	    OwnBoxesService::updateAvailableItemsCache();
	    return $item;
    }

    /**
     * Method, which updates items cache
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function updateItemsCache()
    {
        $items = Item::all();
        \Cache::put(CacheLabels::ItemsList, $items, 30);

        return $items;
    }
}