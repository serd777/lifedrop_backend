<?php namespace App\Http\Services\Prototypes;

/**
 * Fields for user's address
 * @package App\Http\Services
 */
class NewAddress
{
    /**
     * @var string
     */
    var $name;
    /**
     * @var string
     */
    var $country;
    /**
     * @var string
     */
    var $region;
    /**
     * @var string
     */
    var $city;
    /**
     * @var string
     */
    var $address;
    /**
     * @var string
     */
    var $post_index;
    /**
     * @var boolean
     */
    var $has_post_index;
    /**
     * @var string
     */
    var $phone;
    /**
     * @var string
     */
    var $additional_info;
}