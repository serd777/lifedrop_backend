<?php namespace App\Http\Services;

use App\Http\CacheLabels;
use App\Http\Models\Cheat;
use App\Http\Models\Game;
use App\Http\Models\Item;
use App\Http\Models\Profit;
use App\Http\Models\Referral;
use App\Http\Models\User;
use App\Http\Models\UserAddress;
use App\Http\Services\Prototypes\NewAddress;

/**
 * Service for users interactions
 * @package App\Http\Services
 */
class UsersService
{
    /**
     * Method, which returns users
     * @return \Illuminate\Database\Eloquent\Collection|mixed|static[]
     */
    public static function getUsers()
    {
        if(\Cache::has(CacheLabels::UsersList))
            return \Cache::get(CacheLabels::UsersList);
        return self::updateUsersCache();
    }

    /**
     * Method, which returns user's cheats
     * @param $user_id
     * @return mixed
     */
    public static function getUserCheats($user_id)
    {
        return Cheat::ofUser($user_id)->with('item')->get();
    }

    /**
     * Method, which adds user's cheat
     * @param int $user_id
     * @param Item $item
     * @param int $amount
     * @return mixed
     */
    public static function addUserCheat($user_id, $item, $amount)
    {
        $cheat = Cheat::create([
            'user_id' => $user_id,
            'item_id' => $item->id,
            'amount'  => $amount
        ]);
        $cheat->item = $item;

        return $cheat;
    }

    /**
     * Method, which deletes user's cheat
     * @param $id
     */
    public static function deleteUserCheat($id)
    {
        Cheat::where('id', $id)->delete();
    }

    /**
     * Method, which returns user info
     * @param $user_id
     * @return mixed
     */
    public static function getUserInfo($user_id)
    {
        return User::find($user_id);
    }

    /**
     * Method, which returns user's games amount
     * @param $user_id
     * @return mixed
     */
    public static function getUserGamesAmount($user_id)
    {
        if(\Cache::has(CacheLabels::UserGamesAmount . $user_id))
            return \Cache::get(CacheLabels::UserGamesAmount . $user_id);
        return self::updateUserGamesAmount($user_id);
    }

	/**
	 * Method, which returns referral amount
	 * @param $user_id
	 *
	 * @return mixed
	 */
    public static function getUserReferralAmount($user_id)
    {
    	if(\Cache::has(CacheLabels::ReferralsAmount . $user_id))
    		return \Cache::get(CacheLabels::ReferralsAmount . $user_id);
    	return self::updateUserReferralsCount($user_id);
    }

    /**
     * Method, which returns user's profit amount
     * @param $user_id
     * @return mixed
     */
    public static function getUserProfit($user_id)
    {
        if(\Cache::has(CacheLabels::UserProfitAmount . $user_id))
            return \Cache::get(CacheLabels::UserProfitAmount . $user_id);
        return self::updateUserProfitCache($user_id);
    }

    /**
     * Method, which creates updates user's address
     * @param int $user_id
     * @param NewAddress $fields
     * @return UserAddress
     */
    public static function updateUserAddress($user_id, $fields)
    {
        $address = new UserAddress([
            'user_id'         => $user_id,
            'name'            => $fields->name,
            'country'         => $fields->country,
            'region'          => $fields->region,
            'city'            => $fields->city,
            'address'         => $fields->address,
            'post_index'      => $fields->post_index,
            'has_post_index'  => $fields->has_post_index,
            'phone'           => $fields->phone,
            'additional_info' => $fields->additional_info
        ]);
        $address->save();
        return $address;
    }

    /**
     * Method, which updates block state
     * @param $user_id
     * @return mixed
     */
    public static function updateBlockState($user_id)
    {
        $user = User::find($user_id);

        $user->is_blocked =! $user->is_blocked;
        $user->save();

        self::updateUsersCache();
        return $user;
    }

    /**
     * Method, which updates user's permissions
     * @param $user_id
     * @return mixed
     */
    public static function updatePermissions($user_id)
    {
        $user = User::find($user_id);

        $user->is_admin =! $user->is_admin;
        $user->save();

        self::updateUsersCache();
        return $user;
    }

    /**
     * Method, which updates user's balance
     * @param $user_id
     * @param $balance
     * @return mixed
     */
    public static function updateBalance($user_id, $balance)
    {
        $user = User::find($user_id);

        $user->balance = $balance;
        $user->save();

        self::updateUsersCache();
        return $user;
    }

    /**
     * Method, which updates users cache
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function updateUsersCache()
    {
        $users = User::all();
        \Cache::put(CacheLabels::UsersList, $users, 2);

        return $users;
    }

    /**
     * Method, which updates user games amount
     * @param $user_id
     * @return mixed
     */
    public static function updateUserGamesAmount($user_id) {
        $amount = Game::ofUser($user_id)->count();
        \Cache::put(CacheLabels::UserGamesAmount . $user_id, $amount, 120);

        return $amount;
    }

    /**
     * Method, which updates user profit amount
     * @param $user_id
     * @return mixed
     */
    public static function updateUserProfitCache($user_id) {
        $amount = Profit::ofUser($user_id)->sum('profit');
        \Cache::put(CacheLabels::UserProfitAmount . $user_id, $amount, 120);

        return $amount;
    }

	/**
	 * Method, which updates user referrals count
	 * @param $user_id
	 *
	 * @return mixed
	 */
    public static function updateUserReferralsCount($user_id)
    {
    	$amount = Referral::ofMentor($user_id)->count();
    	\Cache::put(CacheLabels::ReferralsAmount . $user_id, $amount);

    	return $amount;
    }
}