<?php namespace App\Http\Services;

use App\Http\CacheLabels;
use App\Http\Models\Drop;

class DropsService
{
    public static function getDrops($user_id) {
        if(\Cache::has(CacheLabels::UserDropsList . $user_id))
            return \Cache::get(CacheLabels::UserDropsList . $user_id);
        return self::updateDropsCache($user_id);
    }

    public static function updateDropsCache($user_id) {
        $drops = Drop::ofUser($user_id)
            ->with('item')
            ->orderBy('created_at', 'desc')
            ->get();
        \Cache::put(CacheLabels::UserDropsList . $user_id, $drops, 60);
        return $drops;
    }
}