<?php namespace App\Http\Services;

use App\Http\CacheLabels;
use App\Http\Models\PromoOffer;

/**
 * Service for promo offers
 * @package App\Http\Services
 */
class PromoOfferService
{
    /**
     * Method, which returns promo offers
     * @return mixed
     */
    public static function getPromoOffers()
    {
        if(\Cache::has(CacheLabels::PromoOffersList))
            return \Cache::get(CacheLabels::PromoOffersList);
        return self::updatePromoOffersCache();
    }

    /**
     * Method, which adds new promo offer
     * @param string $title
     * @param string $text
     * @param $img
     * @return mixed
     */
    public static function addPromoOffer($title, $text, $pos, $img)
    {
        $offer = PromoOffer::create([
            'title' => $title,
            'about' => $text,
	        'pos'   => $pos
        ]);

        $file_path = 'images/offers/' . md5($offer->id . '-offer') . '.png';

        $file = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $img));
        \File::put($file_path, $file);

        $offer->img_path = '/' . $file_path;
        $offer->save();

        self::updatePromoOffersCache();
        return $offer;
    }

    /**
     * Method, which deletes promo offer from DB
     * @param $offer_id
     */
    public static function deletePromoOffer($offer_id)
    {
        PromoOffer::where('id', $offer_id)->delete();
        self::updatePromoOffersCache();
    }

    /**
     * Method, which updates promo offer in DB
     * @param PromoOffer $offer
     * @param string $title
     * @param string $text
     * @return PromoOffer
     */
    public static function updatePromoOffer($offer, $title, $text, $pos)
    {
        $offer->title = $title;
        $offer->about = $text;
        $offer->pos = $pos;
        $offer->save();

        self::updatePromoOffersCache();
        return $offer;
    }

    /**
     * Method, which updates promo offer image
     * @param PromoOffer $offer
     * @param $img
     * @return PromoOffer
     */
    public static function updatePromoOfferImage($offer, $img)
    {
        $time = \Carbon\Carbon::now()->toDateTimeString();
        $file_path = 'images/offers/' . md5($offer->id . '-offer-' . $time) . '.png';

        $file = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $img));
        \File::put($file_path, $file);

        $offer->img_path = '/' . $file_path;
        $offer->save();

        self::updatePromoOffersCache();
        return $offer;
    }

    /**
     * Method, which updates promo offers cache
     * @return mixed
     */
    public static function updatePromoOffersCache()
    {
        $offers = PromoOffer::orderBy('pos', 'asc')->get();
        \Cache::put(CacheLabels::PromoOffersList, $offers, 3600);

        return $offers;
    }
}