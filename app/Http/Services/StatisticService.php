<?php namespace App\Http\Services;

use App\Http\Models\Drop;
use App\Http\Models\Game;
use App\Http\Models\Payment;
use App\Http\Models\Profit;
use Carbon\Carbon;

class StatisticService
{
    /**
     * Method, which returns sum of payments
     *
     * @param Carbon $from
     * @return mixed
     */
    public static function getPaymentsSum($from)
    {
        return Payment::ofPayed()
            ->where('created_at', '>=', $from)
            ->sum('amount');
    }

    /**
     * Method, which returns sum of month payments
     * @return mixed
     */
    public static function getMonthPaymentsSum()
    {
        $from = Carbon::now()->startOfMonth();

        return self::getPaymentsSum($from);
    }

    /**
     * Method, which returns sum of week payments
     * @return mixed
     */
    public static function getWeekPaymentsSum()
    {
	    $from = Carbon::now()->startOfWeek();

        return self::getPaymentsSum($from);
    }

    /**
     * Method, which returns sum of today payments
     * @return mixed
     */
    public static function getTodayPaymentsSum()
    {
	    $from = Carbon::now()->startOfDay();

        return self::getPaymentsSum($from);
    }

	/**
	 * Method, which returns sum of payouts
	 *
	 * @param Carbon $from
	 * @return mixed
	 */
	public static function getPayoutsSum($from)
	{
		$drops = Drop::ofRequested()
		             ->with('item')
		             ->where('created_at', '>=', $from)
		             ->get();
		$sum = 0;

		foreach ($drops as $drop)
			$sum += $drop->item->price;

		return $sum;
	}

	/**
	 * Method, which returns sum of month Payouts
	 * @return mixed
	 */
	public static function getMonthPayoutsSum()
	{
		$from = Carbon::now()->startOfMonth();

		return self::getPayoutsSum($from);
	}

	/**
	 * Method, which returns sum of week Payouts
	 * @return mixed
	 */
	public static function getWeekPayoutsSum()
	{
		$from = Carbon::now()->startOfWeek();

		return self::getPayoutsSum($from);
	}

	/**
	 * Method, which returns sum of today Payouts
	 * @return mixed
	 */
	public static function getTodayPayoutsSum()
	{
		$from = Carbon::now()->startOfDay();

		return self::getPayoutsSum($from);
	}

    /**
     * Method, which returns sum of games played today
     * @return mixed
     */
    public static function getGamesPlayedToday()
    {
	    $from = Carbon::now()->startOfDay();

        return Game::where('created_at', '>=', $from)->count();
    }

	/**
	 * Method, which returns sum of donates by days
	 * @return mixed
	 */
    public static function getDonateGroupByDays()
    {
    	return Payment::ofPayed()
	                  ->selectRaw('DAY(created_at) as day, CAST(SUM(amount) AS UNSIGNED) as sum')
	                  ->groupBy('day')
	                  ->whereBetween('created_at', [
	                    Carbon::today()->startOfMonth(),
		                Carbon::today()->endOfMonth(),
	                  ])
	                  ->get();
    }

    public static function getPayoutsGroupByDays()
    {
    	return \DB::table('drops')
	             ->join(
	             	'items',
		             'drops.item_id',
		             '=',
		             'items.id'
	             )
	             ->selectRaw('DAY(drops.created_at) as day, CAST(SUM(items.price) AS UNSIGNED) as sum')
		         ->groupBy('day')
		         ->whereBetween('drops.created_at', [
			        Carbon::today()->startOfMonth(),
			        Carbon::today()->endOfMonth(),
		         ])
		         ->where('drops.status', '=', 'SENT')
		         ->get();
    }
}