<?php

namespace App\Console\Commands;

use App\Http\Models\Drop;
use App\Http\Models\Game;
use App\Http\Models\Profit;
use App\Http\Models\TopUser;
use App\Http\Models\User;
use Illuminate\Console\Command;

class UpdateTopList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:top_list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command, which updates top users list';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        TopUser::truncate();

        $users = User::all();
        foreach($users as $user) {
        	$games = Game::ofUser($user->id)->count();
        	$profit = Profit::ofUser($user->id)->sum('profit');

        	TopUser::create([
        		'user_id' => $user->id,
		        'games'   => $games,
		        'profit'  => $profit,
	        ]);
        }
    }
}
