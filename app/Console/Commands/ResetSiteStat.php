<?php

namespace App\Console\Commands;

use App\Http\Models\Cheat;
use App\Http\Models\Drop;
use App\Http\Models\Game;
use App\Http\Models\History\HistoryOwnBox;
use App\Http\Models\History\HistoryPayment;
use App\Http\Models\History\HistoryPromo;
use App\Http\Models\History\HistoryReferrals;
use App\Http\Models\OwnBox;
use App\Http\Models\OwnBoxItem;
use App\Http\Models\Payment;
use App\Http\Models\PayoutComment;
use App\Http\Models\Profit;
use App\Http\Models\PromoCode;
use App\Http\Models\PromoCodeUsed;
use App\Http\Models\Referral;
use App\Http\Models\TopUser;
use App\Http\Models\User;
use App\Http\Models\UserAddress;
use Illuminate\Console\Command;

class ResetSiteStat extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reset:stat';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command, which resets site stat';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
	    \DB::statement("SET foreign_key_checks=0");

        HistoryPayment::truncate();
        HistoryPromo::truncate();
        HistoryReferrals::truncate();
        HistoryOwnBox::truncate();

        Referral::truncate();
        PromoCodeUsed::truncate();
        PromoCode::truncate();
        Profit::truncate();
        PayoutComment::truncate();
        OwnBoxItem::truncate();
        OwnBox::truncate();
        Game::truncate();
        Drop::truncate();
        TopUser::truncate();
        Cheat::truncate();
        Payment::truncate();
        UserAddress::truncate();
        User::truncate();

        \DB::statement("SET foreign_key_checks=1");
    }
}
